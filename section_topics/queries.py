#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A set of `Spark-flavoured <https://spark.apache.org/docs/3.1.2/sql-ref-syntax.html>`_ SQL queries
that gather relevant data from the Wikimedia Foundation's
`Analytics Data Lake <https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake>`_.
"""


#: Gather pages with their wikitext.
PAGES = """SELECT wiki_db, revision_id, page_id, REPLACE(page_title, ' ', '_') AS page_title, revision_text
FROM wmf.mediawiki_wikitext_current
WHERE snapshot='{monthly}' AND wiki_db IN ({wikis}) AND page_namespace={namespace} AND page_redirect_title=''
"""

#: Gather Wikidata items with their page links.
QIDS = """SELECT wiki_db, item_id, page_id
FROM wmf.wikidata_item_page_link
WHERE snapshot='{weekly}' AND wiki_db IN ({wikis}) AND page_namespace={namespace}
"""

#: Gather page redirects.
REDIRECTS = """SELECT wiki_db, REPLACE(page_title, ' ', '_') AS page_title,
REPLACE(page_redirect_title, ' ', '_') AS page_redirect_title
FROM wmf.mediawiki_wikitext_current
WHERE snapshot='{monthly}' AND wiki_db IN ({wikis}) AND page_namespace=0 AND page_redirect_title!=''
"""
