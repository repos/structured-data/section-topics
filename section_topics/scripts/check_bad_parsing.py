#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# NOTE Run me from my parent directory
# TODO consider page_title instead of revision_id

"""Check bad section parsing against the Action API via HTTP.

Given a snapshot date and one or more wikis, for each page revision:

- get sections according to :mod:`mwparserfromhell`, by parsing the wikitext
- get sections according to the Action API through a HTTP request
- compare the amount of sections. If there's a difference, emit a *(wiki, revision ID)* tuple
- write a parquet to HDFS
"""

import argparse

import mwparserfromhell as mwpfh
import requests

from section_topics import pipeline


API_URL = (
    'https://{}.wikipedia.org/w/api.php?action=parse&prop=sections&format=json&oldid={}'
)
HTTP_HEADER = {'User-Agent': 'WMF Structured Data team - sd-alerts@lists.wikimedia.org'}


def is_badly_parsed_row(row):
    # Same behavior as the `parse` UDF
    parsed = mwpfh.parse(row.revision_text).get_sections(levels=[2], include_lead=True)
    mwp = len(parsed)

    try:
        response = requests.get(
            API_URL.format(row.wiki_db.rstrip('wiki'), row.revision_id),
            headers=HTTP_HEADER,
        )
    except:
        print('API not ok, skipping:', row.wiki_db, row.page_title, row.revision_id)
        return False

    if not response.ok:
        print(
            'Response not ok, skipping:',
            response.status_code,
            row.wiki_db,
            row.page_title,
            row.revision_id,
        )
        return False

    try:
        json_response = response.json()
    except:
        print('No JSON, skipping:', row.wiki_db, row.page_title, row.revision_id)
        return False

    # Assume that every page has one lead section
    api, no_level = 1, 0

    try:
        sections = json_response['parse']['sections']
        for section in sections:
            level = section.get('toclevel')
            if level is None:
                print(
                    'Section with no level, skipping'
                )  # NOTE this might bias the check
                no_level += 1
                continue
            if level == 1:
                api += 1
    except KeyError:
        print('No sections found:', row.wiki_db, row.page_title, row.revision_id)

    if no_level > 0:
        print(
            no_level,
            'sections with no level:',
            row.wiki_db,
            row.page_title,
            row.revision_id,
        )

    return mwp != api


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Check bad section parsing against the Action API'
    )
    parser.add_argument('snapshot', help='snapshot date (YYYY-MM)')
    parser.add_argument(
        'wiki', nargs='+', help='wiki to check. You can pass multiple ones'
    )
    parser.add_argument(
        '-o', '--output', required=True, help='HDFS path to output dataset'
    )

    return parser.parse_args()


def main() -> None:
    args = parse_args()

    # Uncomment for prod
    from pyspark.sql import SparkSession

    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_session
    # spark = create_session(
    #     app_name='bad-section-parsing-check', type='yarn-large', ship_python_env=True,
    #     extra_settings={
    #         'spark.sql.shuffle.partitions': 2048,
    #         'spark.reducer.maxReqsInFlight': 1,
    #         'spark.shuffle.io.retryWait': '60s',
    #         'spark.executor.memoryOverhead': '2g',
    #         'spark.driver.memory': '16g',
    #         'spark.driver.maxResultSize': '12g',
    #     }
    # )

    snapshot = args.snapshot
    wikis = args.wiki
    output = args.output.format(snapshot)

    all_pages = pipeline.load_pages(
        spark,
        snapshot,
        ', '.join([f"'{w}'" for w in wikis]),
        pipeline.ARTICLE_NAMESPACE,
    )
    bad_pages = all_pages.rdd.filter(is_badly_parsed_row).toDF(all_pages.schema)

    total = all_pages.count()
    bad = bad_pages.count()
    print('Total pages:', total, 'Bad:', bad, 'Ratio:', bad / total if total > 0 else 0)

    bad_pages.select('wiki_db', 'revision_id').write.parquet(output)

    spark.stop()


if __name__ == '__main__':
    main()
