#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# NOTE Run me from my parent directory

"""Collect canonical and localized namespace prefixes of media pages from all Wikipedias, such as ``File:``."""

import argparse
import gzip
import json

from importlib import resources
from io import BytesIO

import requests

from section_topics import pipeline


URL = 'https://dumps.wikimedia.org/{lang}wiki/latest/{lang}wiki-latest-siteinfo-namespaces.json.gz'
HTTP_HEADER = {'User-Agent': 'WMF Structured Data team - sd-alerts@lists.wikimedia.org'}
OUTPUT = pipeline.MEDIA_PREFIXES_PARQUET


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Collect canonical and localized namespace prefixes of media pages from all Wikipedias'
    )

    parser.add_argument(
        '-o',
        '--output',
        metavar='/path/to/file',
        default=OUTPUT,
        help=f'HDFS output path for parquet. Default: "{OUTPUT}"',
    )

    return parser.parse_args()


def main():
    args = parse_args()

    # Uncomment for prod
    from pyspark.sql import SparkSession

    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_session
    # spark = create_session(app_name='collect_media_prefixes', ship_python_env=True)

    with resources.open_text('section_topics.data', 'wikipedias.txt') as win:
        wikis = [line.rstrip('\n') for line in win]

    processed, skipped = 0, 0
    prefixes = set()

    for wiki in wikis:
        try:
            response = requests.get(
                URL.format(lang=wiki.rstrip('wiki')), headers=HTTP_HEADER
            )
        except:
            print(f'API not ok for {wiki}: skipping')
            skipped += 1
            continue

        if not response.ok:
            print(f'Response not ok for {wiki}: skipping')
            skipped += 1
            continue

        try:
            with gzip.open(BytesIO(response.content), 'rt') as gin:
                json_content = json.load(gin)
        except:
            print(f'No GZIP response for {wiki}: skipping')
            skipped += 1
            continue

        try:
            canonical = json_content['query']['namespaces']['6']
            prefixes.add(canonical['canonical'])
            prefixes.add(canonical['*'])
        except KeyError:
            print(f'No canonical prefixes for {wiki}')

        try:
            aliases = json_content['query']['namespacealiases']
            for alias in aliases:
                if alias['id'] == 6:
                    prefixes.add(alias['*'])
        except KeyError:
            print(f'No alias prefixes for {wiki}')

        processed += 1

    print('Total wikis with media prefixes:', processed, '- Skipped:', skipped)
    print()

    df = spark.createDataFrame(
        [[p.replace(' ', '_')] for p in sorted(list(prefixes))],
        schema=[
            'prefix',
        ],
    )
    df.write.parquet(args.output, mode='overwrite')
    spark.stop()


if __name__ == '__main__':
    main()
