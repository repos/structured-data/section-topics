#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pip install mwparserfromhtml

"""Detect Wikipedia article sections with HTML tables.

Given one or more Wikipedias:

- parse the Enterprise HTML dump into sections
- find ``<table>`` tags with no ``role="presentation"`` attributes and at least 3 rows
- as soon as one such table is found in a section,
  emit a *(wiki_db, page_id, page_title, section_index, section_title)* row
- write a parquet to HDFS

Beware! Processing a HTML dump can take days.
"""

import argparse
import subprocess

from bs4 import BeautifulSoup
from mwparserfromhtml import HTMLDump

from section_topics import pipeline


TARGET_WIKIS = ('arwiki', 'bnwiki', 'cswiki', 'eswiki', 'idwiki', 'ptwiki', 'ruwiki')
DUMP_PATH = (
    'section_topics/dumps/{snapshot}/{wiki}-NS0-{snapshot}-ENTERPRISE-HTML.json.tar.gz'
)
OUTPUT = pipeline.TABLE_FILTER_PARQUET


def generate_dataset(dumps_path, snapshot, wikis):
    for wiki in wikis:
        wiki_dump_path = dumps_path.format(snapshot=snapshot, wiki=wiki)
        cat = subprocess.Popen(
            ['hdfs', 'dfs', '-cat', wiki_dump_path], stdout=subprocess.PIPE
        )
        html_dump = HTMLDump(filepath=wiki_dump_path, fileobj=cat.stdout)
        total, skipped, detected = 0, 0, 0

        for article in html_dump:
            article_title = article.get_title().replace(' ', '_')
            html = article.document['article_body']['html']

            if not html:
                print('Skipping unexpected article with no HTML:', article_title)
                skipped += 1
                continue

            try:
                article_id = article.get_page_id()
            except KeyError:
                print('Skipping unexpected article with no page ID:', article_title)
                skipped += 1
                continue

            soup = BeautifulSoup(html, 'html.parser')
            # Extract `<h{N}>` nodes, where `{N}` is the same section level as in the pipeline
            section_headers = soup.select(f'section h{pipeline.SECTION_LEVEL}')

            if section_headers:
                for i, header in enumerate(section_headers, 1):
                    # use the header id/anchor instead of text, it is what we (try to) normalize
                    # to in pipeline.wikitext_headings_to_anchors
                    section_title = header['id']
                    # Go back to the `<section>` parent
                    section_node = header.find_parent('section')
                    # Extract `<table>` nodes
                    # NOTE Strong heuristic: skip tables with `presentation` Aria roles, likely not tabular data
                    tables = section_node.find_all(
                        'table', role=lambda r: r != 'presentation'
                    )
                    if tables:
                        for table in tables:
                            rows = table.find_all('tr')
                            # NOTE Strong heuristic: tabular data is a table with >= 3 rows
                            if len(rows) >= 3:
                                detected += 1
                                yield {
                                    'wiki_db': wiki,
                                    'page_id': article_id,
                                    'page_title': article_title,
                                    'section_index': i,
                                    'section_title': section_title,
                                }
                                break  # Exit after the first detection

            total += 1

        print(
            f'Wiki: {wiki} - Total articles: {total} - Skipped: {skipped} - Sections with data tables: {detected}'
        )


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Detect article sections with tables from Wikimedia Enterprise HTML dumps'
    )
    parser.add_argument('snapshot', metavar='YYYYMMDD', help='snapshot date')
    parser.add_argument(
        '-w',
        '--wikis',
        nargs='+',
        metavar='LANGwiki',
        default=TARGET_WIKIS,
        help=f'input wiki(s), separated by space. Default: {" ".join(TARGET_WIKIS)}',
    )
    parser.add_argument(
        '-d',
        '--dumps-path',
        metavar='/path/to/dumps',
        default=DUMP_PATH,
        help=(
            'HDFS path mask to HTML dumps. {wiki} and {snapshot} will be interpolated. '
            f'Default: "{DUMP_PATH}"',
        ),
    )
    parser.add_argument(
        '-o',
        '--output',
        metavar='/path/to/file',
        default=OUTPUT,
        help=f'HDFS output path for parquet. Default: "{OUTPUT}"',
    )

    return parser.parse_args()


def write(spark, output, data, mode):
    df = spark.createDataFrame(data)
    df.write.parquet(output, mode=mode)


def main():
    args = parse_args()

    # Uncomment for prod
    from pyspark.sql import SparkSession

    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_session
    # spark = create_session(app_name='detect_html_tables', ship_python_env=True)

    snapshot = args.snapshot
    wikis = args.wikis
    dumps_path = args.dumps_path
    output = args.output.format(snapshot=snapshot)

    dataset = generate_dataset(dumps_path, snapshot, wikis)

    mode = 'overwrite'
    batch_size = 10000
    batch = []
    for i, row in enumerate(dataset):
        batch.append(row)
        if i % batch_size == batch_size - 1:
            write(spark, output, batch, mode)
            batch = []
            # after the first batch has been stored to a new parquet, switch to append for all following
            mode = 'append'
    if len(batch) > 0:
        write(spark, output, batch, mode)

    spark.stop()


if __name__ == '__main__':
    main()
