# -*- coding: utf-8 -*-
# pip install sparqlwrapper
# https://rdflib.github.io/sparqlwrapper/

"""Fetches QIDs from Wikidata via Query Service."""

import argparse
import ssl
import sys

from SPARQLWrapper import JSON, SPARQLWrapper


# prevent SSL certificate verification
# @todo figure out how to install certificates where this is run
ssl._create_default_https_context = ssl._create_unverified_context

endpoint_url = 'https://query.wikidata.org/sparql'

# Example usage:
# $ python scripts/fetch_qids_from_wikidata.py scripts/sparql/qids_for_all_points_in_time.sparql --output=section_topics/qids_for_all_points_in_time


def get_results(url, query):
    user_agent = 'section-topics-one-off-script/%s.%s' % (
        sys.version_info[0],
        sys.version_info[1],
    )
    sparql = SPARQLWrapper(url, agent=user_agent)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Execute a Sparql query against Wikidata, outputting a list of item Q-ids'
    )

    parser.add_argument('sparql', help='Path to sparql query to execute')
    parser.add_argument('-o', '--output', help=f'HDFS output path for parquet.')

    return parser.parse_args()


def main():  # pragma: no cover
    args = parse_args()

    # Uncomment for prod
    from pyspark.sql import SparkSession

    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_session
    # spark = create_session(app_name='fetch_qids_from_wikidata', ship_python_env=True)

    with open(args.sparql) as f:
        query = f.read()

    results = get_results(endpoint_url, query)
    # get values from JSON
    results = [result['item']['value'] for result in results['results']['bindings']]
    # strip URI
    results = [result.lstrip('https://www.wikidata.org/entity/') for result in results]
    # Keep only items Q items
    results = filter(lambda q: q.startswith('Q'), results)
    # Make elements unique
    results = set(results)
    # sort based on the int part for easy comparison when this result changes
    results = sorted(results, key=lambda r: int(r.lstrip('Q')))

    # now write results to dataframe
    df = spark.createDataFrame([[result] for result in results], schema=['topic_qid'])
    df.write.parquet(args.output, mode='overwrite')
    spark.stop()


if __name__ == '__main__':
    main()
