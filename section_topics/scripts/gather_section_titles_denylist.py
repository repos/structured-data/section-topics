#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Gather section titles to be excluded at section extraction time,
see :func:`section_topics.pipeline.parse_excluding`.

Given a dict of ``{source language: set of section titles}``,
use the section alignment datasets to gather a denylist in all available target languages.
The target language title is the top-ranked alignment.
"""

import argparse
import json

from collections import OrderedDict, defaultdict
from importlib import resources

import requests

from pyspark.sql import types as T
from pyspark.sql.utils import AnalysisException

from section_topics import pipeline


# Titles are intentionally redundant across languages: sets will avoid duplicates
EXCLUDE = defaultdict(
    set,
    {
        # https://phabricator.wikimedia.org/T318092
        'enwiki': set(
            [
                'external links',
                'further reading',
                'references',
                'description',
                'economy',
                'culture',
                'demography',
                'etymology',
            ]
        ),
        # https://phabricator.wikimedia.org/T279519#7368694
        'frwiki': set(
            [
                'notes',
                'références',
                'notes et références',
                'bibliographie',
                'webographie',
                'articles connexes',
                'liens externes',
                'voir aussi',
                'annexes',
            ]
        ),
        'eswiki': set(
            [
                'notas',
                'referencias',
                'enlaces externos',
                'véase también',
                'bibliografía',
                'fuentes',
                'libros',
                'periódicos y publicaciones',
                'en línea',
                'demografía',
                'descripción',
                'formación y yacimientos',
            ]
        ),
    },
)

# Per-wiki community-curated page with excluded sections,
# see https://phabricator.wikimedia.org/T311730#8341495
DENYLIST_PAGE = 'MediaWiki:NewcomerTasks.json'
API_URL = 'https://{}.wikipedia.org/w/api.php'
HTTP_HEADER = {'User-Agent': 'WMF Structured Data team - sd-alerts@lists.wikimedia.org'}
PARAMS = {
    'action': 'query',
    'format': 'json',
    'prop': 'revisions',
    'titles': DENYLIST_PAGE,
    'rvprop': 'content',
    'rvslots': 'main',
    'formatversion': 2,
}

OUTPUT = pipeline.SECTIONS_DENYLIST_PARQUET


def collect_community_denylists(wikis):
    processed, skipped = 0, 0

    for wiki in wikis:
        try:
            response = requests.get(
                API_URL.format(wiki.rstrip('wiki')), headers=HTTP_HEADER, params=PARAMS
            )
        except:
            print(f'API not ok for {wiki}: skipping')
            skipped += 1
            continue

        if not response.ok:
            print(f'Response not ok for {wiki}: skipping')
            skipped += 1
            continue

        try:
            json_response = response.json()
        except:
            print(f'No JSON response for {wiki}: skipping')
            skipped += 1
            continue

        try:
            content = json.loads(
                json_response['query']['pages'][0]['revisions'][0]['slots']['main'][
                    'content'
                ]
            )
        except (KeyError, IndexError, json.JSONDecodeError):
            print(f'Malformed JSON for {wiki}: skipping')
            skipped += 1
            continue

        try:
            denylist = content['link-recommendation']['excludedSections']
        except KeyError:
            skipped += 1
            continue

        EXCLUDE[wiki].update(set(denylist))
        processed += 1

    print('Total wikis with denylist:', processed, '- Skipped:', skipped)
    print()


def populate_denylist(spark_session, aligned_sections_path):
    denylist = defaultdict(set)

    for source, sections in EXCLUDE.items():
        normalized_sections_df = spark_session.createDataFrame(
            [{'section': v} for v in sections],
            T.StructType([T.StructField('section', T.StringType(), nullable=False)]),
        ).withColumn('section', pipeline.normalize_heading_column('section'))

        # Add source language titles
        for row in normalized_sections_df.collect():
            denylist[source].add(row.section)

        # Add section alignments
        try:
            ddf = spark_session.read.parquet(aligned_sections_path.format(source))
        except AnalysisException:
            print(f'Section alignments not available for {source}: skipping')
            continue

        translations = (
            ddf.where(
                pipeline.normalize_heading_column('source').isin(denylist[source])
            )
            .where(ddf.rank == 1)
            .where(ddf.source_language == source)
            .select('target', 'target_language')
        )

        for row in translations.collect():
            denylist[row.target_language].add(row.target)

    # Make values serializable and sort sections & wiki dbnames
    ordered_sections = {
        wiki: sorted(list(sections)) for wiki, sections in denylist.items()
    }
    return OrderedDict(sorted(ordered_sections.items()))


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Gather section titles to be excluded at section extraction time'
    )
    # Required
    parser.add_argument(
        '-a',
        '--aligned-sections',
        required=True,
        metavar='/hdfs_path/to/parquet',
        help=(
            'HDFS path to parquet of aligned sections. '
            'This can be either one parquet for all wikis like `/my/full-parquet`, '
            'or a per-wiki templated path like `/my/{}-parquet` to be filled with the wiki DB name'
        ),
    )
    # Optional
    parser.add_argument(
        '-w',
        '--wikis',
        metavar='/path/to/file.txt',
        type=argparse.FileType('r'),
        default=resources.open_text('section_topics.data', 'wikipedias.txt'),
        help='Plain text file of wikis to process, one per line. Default: all Wikipedias, see "data/wikipedias.txt"',
    )
    parser.add_argument(
        '-o',
        '--output',
        metavar='/hdfs_path/to/parquet',
        default=OUTPUT,
        help=f'HDFS path to output parquet. Default: "{OUTPUT}"',
    )

    return parser.parse_args()


def main():
    args = parse_args()

    # Uncomment for prod
    from pyspark.sql import SparkSession

    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_session
    # spark = create_session(app_name='gather_section_titles_denylist', ship_python_env=True)

    # Exhaustively search input wikis
    with args.wikis as win:
        wikis = [line.rstrip('\n') for line in win]

    collect_community_denylists(wikis)
    denylist = populate_denylist(spark, args.aligned_sections)

    df = spark.createDataFrame(
        [
            [wiki, section]
            for wiki, sections in denylist.items()
            for section in sections
        ],
        schema=['wiki_db', 'section_heading'],
    )
    df.write.parquet(args.output, mode='overwrite')

    spark.stop()


if __name__ == '__main__':
    main()
