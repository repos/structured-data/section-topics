#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate image suggestions for Wikipedia article sections:
this is the section alignment image suggestions data pipeline.

Inputs:

- Wikipedia article section images as output by :ref:`pipeline`
- machine-learned `section alignment <https://gitlab.wikimedia.org/repos/structured-data/seal>`_
  dataset of *(source, target)* section title pairs

High-level steps given a target section:

- skip it if it has more images than a threshold
- gather all equivalent source pages with the same Wikidata
  `QID <https://www.wikidata.org/wiki/Wikidata:Glossary#QID>`_ as the target page
- filter out irrelevant images from the source sections,
  namely icons or those appearing in the target too
- project all source section images to the target
- combine suggestion candidates

Output :class:`pyspark.sql.DataFrame` row example:

=========  ==============  =======  ============  ============  ===============================================================  ==============
target_id  target_heading  item_id  target_title  target_index  recommended_images                                               target_wiki_db
=========  ==============  =======  ============  ============  ===============================================================  ==============
1485753    Biografia       Q317475  Ali_Shariati  1             [{frwiki -> [Shariati7.jpg]}, {ruwiki -> [Shariati7.jpg]}, ...]  ptwiki
=========  ==============  =======  ============  ============  ===============================================================  ==============

More `documentation <https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Section-level_Image_Suggestions/Data_Pipeline#How_it_works>`_
lives in MediaWiki.
"""

import argparse
import json
import string

from dataclasses import dataclass
from functools import partial
from importlib import resources
from itertools import product
from typing import Any, List, Sequence, Set, Tuple

import pandas as pd

from pyspark.sql import Column
from pyspark.sql import DataFrame as SparkDataFrame
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T


# The usual output size with no `coalesce` is roughly 409 files per partition:
# 114k files across 278 directories (read wikis, as we previously partitioned by them).
# Decrease the size by an order of magnitude.
COALESCE = 40
# Map table filter columns to output dataset ones
TABLE_FILTER_COLUMNS_MAPPING = {
    'wiki_db': 'target_wiki_db',
    'page_id': 'target_id',
    'section_title': 'target_heading',
}
# ASCII punctuation characters to be stripped from section titles.
# Include the ASCII white space, don't strip round brackets.
STRIP_CHARS = string.punctuation.replace('()', ' ')
# All kinds of white space to be substituted for the ASCII one;
# underscores turn into spaces as well
SUBSTITUTE_PATTERN = r'[\s_]'


@dataclass
class SectionImages:
    index: int
    heading: str
    links: List[str]  # Output by section topics, unused
    images: List[str]


@dataclass
class Page:
    """:func:`dataclasses.dataclass` that stores a Wikipedia article page
    and its available images.

    :param item_id: a page Wikidata QID
    :param page_id: a page ID
    :param page_title: a page title
    :param wiki_db: a page wiki
    :param section_images: a list of section images
    """

    item_id: str
    page_id: int
    page_title: str
    wiki_db: str
    section_images: List[SectionImages]

    @property
    def all_images(self) -> List[str]:
        return [image for s in self.section_images for image in s.images]


@dataclass
class Recommendation:
    """:func:`dataclasses.dataclass` that stores image suggestions
    for a target Wikipedia article page.

    :param item_id: a page Wikidata QID
    :param target_id: a page ID
    :param target_title: a page title
    :param target_index: a section numerical index
    :param target_heading: a section heading
    :param target_wiki_db: a page wiki
    :param source_heading: a section heading where suggestions come from
    :param source_wiki_db: a page wiki where suggestions come from
    :param recommended_images: a list of suggested image file names
    """

    item_id: str
    target_id: int
    target_title: str
    target_index: int
    target_heading: str
    target_wiki_db: str
    source_heading: str
    source_wiki_db: str
    recommended_images: List[str]


recommendation_schema = T.StructType(
    [
        T.StructField('item_id', T.StringType(), nullable=False),
        T.StructField('target_id', T.IntegerType(), nullable=False),
        T.StructField('target_title', T.StringType(), nullable=False),
        T.StructField('target_index', T.IntegerType(), nullable=False),
        T.StructField('target_heading', T.StringType(), nullable=False),
        T.StructField('target_wiki_db', T.StringType(), nullable=False),
        T.StructField('source_heading', T.StringType(), nullable=False),
        T.StructField('source_wiki_db', T.StringType(), nullable=False),
        T.StructField(
            'recommended_images', T.ArrayType(T.StringType()), nullable=False
        ),
    ]
)


def field_names(class_or_instance: Any) -> List[str]:
    try:
        fields = getattr(class_or_instance, '__dataclass_fields__')
    except AttributeError:
        raise TypeError('must be called with a dataclass type or instance')
    return [f.name for f in fields.values()]


def load(article_images: str) -> List[SectionImages]:
    """Convert section images from an :ref:`images`'s output row
    to a :class:`imagerec.article_images.SectionImages` instance.

    :param article_images: a stringified JSON array of section images.
      Corresponds to a value of the ``article_images`` column in
      :ref:`images`'s output :class:`pyspark.sql.DataFrame`
    :return: the list of converted section images
    """
    section_images = json.loads(article_images)
    return list(map(lambda s: SectionImages(**s), section_images))


def rows_to_pages(df: pd.DataFrame) -> List[Page]:
    """Convert all input :class:`pandas.DataFrame` rows to
    :class:`Page` instances.

    :param df: an in-memory dataframe loaded from
      :ref:`images`'s output :class:`pyspark.sql.DataFrame`
    :return: the list of pages
    """
    rows = df.to_records(index=False)
    return [
        Page(
            item_id=row['item_id'],
            page_id=row['page_id'],
            page_title=row['page_title'],
            wiki_db=row['wiki_db'],
            section_images=load(row['article_images']),
        )
        for row in rows
    ]


def filter_source_images(
    source_images: Sequence[str], target_images: Set[str]
) -> List[str]:
    """Filter out source images that either appear in target ones
    or are icons/indicators.

    Icon and indicator file names have a ``OOjs_UI_icon`` and
    ``OOjs_UI_indicator`` prefix respectively.

    :param source_images: a sequence of source image file names
    :param target_images: a set of target image file names
    :return: the list of filtered source image file names
    """
    return [
        img
        for img in source_images
        if not (img in target_images or img.startswith('OOjs_UI'))
    ]


def make_recommendations(
    target_page: Page, source_page: Page, max_target_images: int
) -> List[Recommendation]:
    """Generate all image suggestion candidates for a target page via
    the `Cartesian product <https://en.wikipedia.org/wiki/Cartesian_product>`_
    of all *(target, source)* section pairs.

    Filter source images through :func:`filter_source_images`.
    Filter target sections with more images than a given threshold.

    :param target_page: a target page
    :param source_page: a source page
    :param max_target_images: a maximum amount of images
      for a target section to be kept
    :return: the list of image suggestion candidates
    """
    target_images = set(target_page.all_images)
    # Filter out sections with more images than `max_target_images`
    target_section_images = (
        section_images
        for section_images in target_page.section_images
        if len(section_images.images) <= max_target_images
    )
    # Cartesian product of all sections in `target_page` and `source_page`.
    # Remember that lead sections are excluded from `article_images` output.
    return [
        Recommendation(
            item_id=target_page.item_id,
            target_id=target_page.page_id,
            target_title=target_page.page_title,
            target_index=s1.index,
            target_heading=s1.heading,
            target_wiki_db=target_page.wiki_db,
            source_heading=s2.heading,
            source_wiki_db=source_page.wiki_db,
            recommended_images=filter_source_images(s2.images, target_images),
        )
        for s1, s2 in product(target_section_images, source_page.section_images)
    ]


def combine_pages(
    target_pages: Sequence[Page], source_pages: Sequence[Page]
) -> List[Tuple[Page, Page]]:
    """Generate all *(target, source)* page pairs via their Cartesian product.

    Ensure that no pair has the same language.

    :param target_pages: a sequence of target pages
    :param source_pages: a sequence of source pages
    :return: the list of *(target, source)* page pairs
    """
    return [
        (target_page, source_page)
        for target_page, source_page in product(target_pages, source_pages)
        if target_page.wiki_db != source_page.wiki_db
    ]


def generate_image_recommendations(
    target_wiki_dbs: Sequence[str], max_target_images: int, df: pd.DataFrame
) -> pd.DataFrame:
    """Generate all possible suggestions for pages in the target wikis.

    Filter out empty ones.

    :param target_wiki_dbs: a sequence of target wikis
    :param max_target_images: a maximum amount of images
      for a target section to be kept
    :param df: an in-memory dataframe of pages
    :return: the in-memory dataframe of image suggestions
    """
    pages = rows_to_pages(df)
    target_pages = [page for page in pages if page.wiki_db in target_wiki_dbs]
    # Make recommendations for each (target, source) page pair
    # and filter out empty ones
    recommendations = [
        recommendation.__dict__
        for p1, p2 in combine_pages(target_pages, pages)
        for recommendation in make_recommendations(p1, p2, max_target_images)
        if recommendation.recommended_images
    ]
    return pd.DataFrame(recommendations, columns=field_names(Recommendation))


def get_image_recommendations(
    section_images_df: SparkDataFrame,
    target_wiki_dbs: Sequence[str],
    max_target_images: int,
) -> SparkDataFrame:
    """Generate the full dataset of all possible suggestions for all sections
    of all pages in the target wikis.

    Corresponding source pages have the same Wikidata QID as a given target page.

    :param section_images_df: a distributed dataframe of section images
    :param target_wiki_dbs: a sequence of target wikis
    :param max_target_images: a maximum amount of images
      for a target section to be kept
    :return: the distributed dataframe of image suggestions
    """
    # A grouped map Pandas UDF takes as input a dataframe and returns a dataframe:
    # we use `partial` to pass additional arguments down to the UDF.
    generate_image_recommendations_udf = F.pandas_udf(
        partial(generate_image_recommendations, target_wiki_dbs, max_target_images),
        recommendation_schema,
        F.PandasUDFType.GROUPED_MAP,
    )

    # Spark uses Arrow to transfer data to Python workers,
    # which is needed to execute the Pandas UDF.
    # Arrow doesn't support nested `StructType`s,
    # so we convert the `article_images` column
    # to JSON before applying the Pandas UDF.
    section_images_df = section_images_df.withColumn(
        'article_images', F.to_json('article_images')
    )
    recommendations_df = section_images_df.groupBy('item_id').apply(
        generate_image_recommendations_udf
    )

    return recommendations_df


# Copied from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/main/section_topics/pipeline.py
def normalize_heading_column(
    column: Column,
    substitute_pattern: str = SUBSTITUTE_PATTERN,
    strip_chars: str = STRIP_CHARS,
) -> Column:
    """Same as :func:`section_topics.pipeline.normalize_heading_column`."""
    # These chars will be surrounded by `[]` to form a regexp group, so escape literal `[` and `]`.
    # Also double the backslash to prevent it from being an escape char itself.
    escaped_strip_chars = (
        strip_chars.replace('\\', '\\\\').replace('[', r'\[').replace(']', r'\]')
    )

    return F.lower(
        F.regexp_replace(
            F.regexp_replace(
                F.regexp_replace(
                    column,
                    '_[0-9]+$',
                    '',
                ),
                substitute_pattern,
                ' ',
            ),
            f'^[{escaped_strip_chars}]*(.*?)[{escaped_strip_chars}]*$',
            '$1',
        )
    )


def process_image_recommendations(
    recommendations_df: SparkDataFrame, alignments_df: SparkDataFrame
) -> SparkDataFrame:
    """Build the final output dataset of image suggestions.

    Combine all suggestion candidates with aligned sections.

    :param recommendations_df: a distributed dataframe of
      all image suggestion candidates
    :param alignments_df: a distributed dataframe of section alignments
    :return: the distributed dataframe of final image suggestions
    """
    recommendations_df = recommendations_df.alias('r')
    alignments_df = alignments_df.alias('a')

    # `target_heading` and `source_heading` come from `article_images`,
    # `source` and `target` from section alignments.
    # The former are in anchor format (original case, markup removed,
    # underscored, numeric suffix for duplicate on same page);
    # the latter in a less standard one (lowercased,
    # spaces, some leading/trailing characters removed).
    # Since the formats don't match, we must normalize them to
    # a common one and accept that some precision may be lost.
    # For instance, section alignment's format don't disambiguate
    # identical section headings, thus leading to wrong joins.
    # However, these are negligible edge cases.
    target_headings_match = normalize_heading_column(
        'r.target_heading'
    ) == normalize_heading_column('a.source')
    source_headings_match = normalize_heading_column(
        'r.source_heading'
    ) == normalize_heading_column('a.target')
    processed_recs_df = recommendations_df.join(
        alignments_df,
        on=((target_headings_match) & (source_headings_match)),
        how='left_semi',
    )

    # Combine all recommended images from each section in
    # `source_wiki_db` for a target heading
    processed_recs_df = processed_recs_df.groupBy(
        [
            'item_id',
            'target_id',
            'target_title',
            'target_index',
            'target_heading',
            'target_wiki_db',
            'source_wiki_db',
        ]
    )
    processed_recs_df = processed_recs_df.agg(
        F.array_distinct(F.flatten(F.collect_list('recommended_images'))).alias(
            'recommended_images'
        )
    )

    # Group all recommended images from each `source_wiki_db` for
    # one target section into one map
    processed_recs_df = processed_recs_df.groupBy(
        [
            'item_id',
            'target_id',
            'target_title',
            'target_index',
            'target_heading',
            'target_wiki_db',
        ]
    )
    processed_recs_df = processed_recs_df.agg(
        F.collect_list(F.create_map('source_wiki_db', 'recommended_images')).alias(
            'recommended_images'
        )
    )
    return processed_recs_df


# Copied from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/3aca597b346cc2bfdef5138ad6ace7fd0e90bdfe/section_topics/pipeline.py#L78
# TODO Refactor into a shared component, see https://phabricator.wikimedia.org/T330841
def apply_filter(
    df: SparkDataFrame, filter_df: SparkDataFrame, broadcast: bool = False
) -> SparkDataFrame:
    """Same as :func:`section_topics.pipeline.apply_filter`."""
    cols = set(df.columns)
    filter_cols = set(filter_df.columns)
    if not filter_cols.issubset(cols):
        raise ValueError(
            f'The filter dataframe has invalid columns: {filter_cols}. They must be a subset of {cols}'
        )

    if broadcast:
        filter_df = F.broadcast(filter_df)

    return df.join(filter_df, on=list(filter_cols), how='left_anti')


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Generate section-level image suggestions based on section alignments'
    )

    # Required
    parser.add_argument(
        '--section-images',
        required=True,
        metavar='/hdfs_path/to/parquet',
        help='HDFS path to parquet of section images, as output by section topics',
    )
    parser.add_argument(
        '--section-alignments',
        required=True,
        metavar='/hdfs_path/to/parquet',
        help='HDFS path to parquet of section alignments',
    )
    parser.add_argument(
        '--max-target-images',
        required=True,
        metavar='N',
        type=int,
        help=(
            'Maximum number of images that a section being'
            'recommended images should contain. Use 0 if you'
            'want to generate recommendations only for unillustrated'
            'sections.'
        ),
    )
    parser.add_argument(
        '--output',
        required=True,
        metavar='/hdfs_path/to/parquet',
        help='HDFS path to output parquet',
    )

    # Optional
    parser.add_argument(
        '-w',
        '--wikis',
        metavar='/path/to/file.txt',
        type=argparse.FileType('r'),
        default=resources.open_text('section_topics.data', 'wikipedias.txt'),
        help=(
            'plain text file of wikis to process, one per line. '
            'Default: all Wikipedias, see "data/wikipedias.txt"'
        ),
    )
    parser.add_argument(
        '--coalesce',
        metavar='N',
        default=COALESCE,
        help=(
            'Control the amount of files per output partition. '
            'A higher value implies more files but a faster and lighter execution. '
            f'Default: {COALESCE}'
        ),
    )

    return parser.parse_args()


def main(args: argparse.Namespace, spark: SparkSession) -> None:  # pragma: no cover
    with args.wikis as win:
        wikis = [w.rstrip() for w in win]

    section_images_df = spark.read.parquet(args.section_images)
    alignments_df = spark.read.parquet(args.section_alignments)

    recommendations_df = get_image_recommendations(
        section_images_df,
        wikis,
        args.max_target_images,
    )
    processed_recs_df = process_image_recommendations(recommendations_df, alignments_df)
    processed_recs_df.coalesce(args.coalesce).write.parquet(
        args.output, mode='overwrite'
    )


if __name__ == '__main__':
    args = parse_args()

    # Uncomment for prod
    from pyspark.sql import SparkSession

    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_session

    # spark = create_session(
    # app_name='section-alignment-image-suggestions',
    # type='yarn-large',
    # ship_python_env=True,
    # extra_settings={
    # 'spark.executor.memory': '8g',
    # 'spark.executor.memoryOverhead': '3g',
    # 'spark.sql.shuffle.partitions': 1024,
    # 'spark.reducer.maxReqsInFlight': 1,
    # 'spark.shuffle.io.retryWait': '60s',
    # 'spark.stage.maxConsecutiveAttempts': 10,
    # },
    # )
    main(args, spark)
    spark.stop()
