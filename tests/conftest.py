#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest

from pyspark.sql import types as T


@pytest.fixture(scope='session')
def pages(spark_session):
    return spark_session.createDataFrame(
        [
            ('eswiki', 42, 666, 'El_Diablo', '#REDIRECCIÓN [[El día de la bestia]]'),
            ('itwiki', 69, 127, 'Fiat_127', 'La Fiat 127 è una [[autovettura]]'),
            ('frwiki', 98, 1407, '14_Juillet', 'Jour de fête nationale de France'),
        ],
        ['wiki_db', 'revision_id', 'page_id', 'page_title', 'revision_text'],
    )


@pytest.fixture(scope='session')
def pages_with_qids(spark_session):
    return spark_session.createDataFrame(
        [
            # Link with first letter lowercased
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                '#REDIRECCIÓN [[el día de la bestia]]',
            ),
            (
                'ptwiki',
                37,
                'Q101',
                101,
                'Presunto',
                'O presunto do porco preto é muito bom\n==one==\n[[Ficheiro:Presunto_de_Monchique.WEBP|thumb|left|x1984px|alt=Presunto de calidade|Imagem de um presunto]]',
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                '{{Infobox livre|auteur=[[George Orwell]]}}\nClassique dystopique\n\n\n==Résumé==\n\n{{Infobox livre|auteur=George|image=1984.tiff}}\n==Personnages==\nMinistère de la [[Vérité]]',
            ),
            (
                'jawiki',
                610,
                'Q1234',
                1234,
                '茎茶',
                'Kukicha is a green tea\n==1==\n{{wide image|tea TEA tea!!!!!.aPnG|610px|[https://external-tea.link]}}\n\n[[カテゴリ:Japanese Teas]]',
            ),
            (
                'arywiki',
                999,
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                'A tajine is a dish named after the pot.\n==1==\n[[ﺖﺼﻨﻴﻓ:Moroccan Dishes]]',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'revision_text',
        ],
    )


@pytest.fixture(scope='session')
def topics_with_dates(spark_session):
    # From sample at https://docs.google.com/spreadsheets/d/1SJ9QDajpiUTGnE9MH0NsxWVYR6SIXWzH1iiwxLSa-so
    return spark_session.createDataFrame(
        [
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                1,
                'Q2550',
                '5_мая',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                2,
                'Q745758',
                'Глостер_(регбийный_клуб)',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                3,
                'Q1149835',
                'Харлекуинс',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                4,
                'Q1201283',
                'Нортгемптон_Сэйнтс',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                5,
                'Q1548720',
                'Ольстер_(регбийный_клуб)',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                6,
                'Q2503',
                '8_апреля',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                7,
                'Q1471265',
                'Перпиньян_(регбийный_клуб)',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                8,
                'Q2961',
                '19_октября',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                9,
                'Q297901',
                'Клермон_Овернь',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                10,
                'Q2986',
                '12_ноября',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                11,
                'Q2920',
                '12_октября',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                12,
                'Q2247',
                '11_января',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                13,
                'Q2221',
                '6_января',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                14,
                'Q2255',
                '13_января',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'section_id',
            'topic_qid',
            'topic_title',
        ],
    )


@pytest.fixture(scope='session')
def topics_with_media(spark_session):
    # From sample at https://docs.google.com/spreadsheets/d/1NvYDeXedpu2TJIrKfOEmEM_eRGhkOORm/edit#gid=1124081039
    return spark_session.createDataFrame(
        [
            # Media
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q40469',
                'Associated_Press',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q1160945',
                'BBC_News',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q40464',
                'Agence_France-Presse',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'background',
                1,
                'Q166180',
                'France_24',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'background',
                1,
                'Q166180',
                'BCC_News',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'background',
                1,
                'Q130879',
                'Reuters',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'background',
                1,
                'Q1665444',
                'The_New_Humanitarian',
            ),
            # Others
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q90',
                'Paris',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q6891134',
                'Mohamed_Djaanfari',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q1041',
                'Senegal',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q158692',
                'Comorian_Armed_Forces',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'section_id',
            'topic_qid',
            'topic_title',
        ],
    )


@pytest.fixture
def lead_section():
    # The expected parsed lead section of https://it.wikipedia.org/wiki/Gaznevada
    # Keys are alphabetically sorted
    return {
        'heading': '### zero ###',
        'images': [],
        'index': 0,
        'links': [
            # Infobox
            "Harpo's_Music",
            'Italian_Records',
            'EMI',
            'CBS_(etichetta_discografica)',
            'Sony',
            # Text
            'gruppo_musicale',
            'Bologna',
            'anni_1970',
            'anni_1980',
            'Skiantos',
            'Hi-Fi_Bros',
            'Confusional_Quartet',
            "Movimento_del_'77",
            'Punk_rock',
            'No_wave',
            'New_wave_(musica)',
            'italo_disco',
            'Shake_Edizioni',
        ],
    }


@pytest.fixture(scope='session')
def sections(spark_session):
    return spark_session.createDataFrame(
        [
            # Link with first letter lowercased
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                0,
                '### zero ###',
                'el_día_de_la_bestia',
                'el_día_de_la_bestia',
            ),
            ('ptwiki', 37, 'Q101', 101, 'Presunto', 0, '### zero ###', None, None),
            (
                'ptwiki',
                37,
                'Q101',
                101,
                'Presunto',
                1,
                'one',
                'Ficheiro:Presunto_de_Monchique.WEBP',
                'Ficheiro:presunto_de_Monchique.WEBP',
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                0,
                '### zero ###',
                'George_Orwell',
                'george_Orwell',
            ),
            ('frwiki', 77, 'Q1984', 1984, '1984', 1, 'Résumé', None, None),
            ('frwiki', 77, 'Q1984', 1984, '1984', 2, 'Personnages', 'Vérité', 'vérité'),
            # Category links
            ('jawiki', 610, 'Q1234', 1234, '茎茶', 0, '### zero ###', None, None),
            (
                'jawiki',
                610,
                'Q1234',
                1234,
                '茎茶',
                1,
                '1',
                'カテゴリ:Japanese_Teas',
                'カテゴリ:Japanese_Teas',
            ),
            ('arywiki', 999, 'Q4321', 4321, 'ﻁﺎﺠﻴﻧ', 0, '### zero ###', None, None),
            (
                'arywiki',
                999,
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                1,
                '1',
                'ﺖﺼﻨﻴﻓ:Moroccan_Dishes',
                'ﺖﺼﻨﻴﻓ:moroccan_Dishes',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'link',
            'normalized_link',
        ],
    )


@pytest.fixture(scope='session')
def parsed(spark_session):
    schema = T.StructType(
        [
            T.StructField('wiki_db', T.StringType(), False),
            T.StructField('revision_id', T.IntegerType(), False),
            T.StructField('page_qid', T.StringType(), False),
            T.StructField('page_id', T.IntegerType(), False),
            T.StructField('page_title', T.StringType(), False),
            T.StructField(
                'parsed',
                T.ArrayType(
                    T.StructType(
                        [
                            T.StructField('index', T.IntegerType(), False),
                            T.StructField('heading', T.StringType(), False),
                            T.StructField('links', T.ArrayType(T.StringType()), False),
                            T.StructField('images', T.ArrayType(T.StringType()), False),
                        ]
                    )
                ),
            ),
        ]
    )

    return spark_session.createDataFrame(
        [
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                [
                    {
                        'index': 0,
                        'heading': '### zero ###',
                        'links': ['el_día_de_la_bestia'],
                        'images': [],
                    },
                ],
            ),
            (
                'ptwiki',
                37,
                'Q101',
                101,
                'Presunto',
                [
                    {'index': 0, 'heading': '### zero ###', 'links': [], 'images': []},
                    {
                        'index': 1,
                        'heading': 'one',
                        'links': ['Ficheiro:Presunto_de_Monchique.WEBP'],
                        'images': ['Presunto_de_Monchique.WEBP'],
                    },
                ],
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                [
                    {
                        'index': 0,
                        'heading': '### zero ###',
                        'links': ['George_Orwell'],
                        'images': [],
                    },
                    {
                        'index': 1,
                        'heading': 'Résumé',
                        'links': [],
                        'images': ['1984.tiff'],
                    },
                    {
                        'index': 2,
                        'heading': 'Personnages',
                        'links': ['Vérité'],
                        'images': [],
                    },
                ],
            ),
            (
                'jawiki',
                610,
                'Q1234',
                1234,
                '茎茶',
                [
                    {'index': 0, 'heading': '### zero ###', 'links': [], 'images': []},
                    {
                        'index': 1,
                        'heading': '1',
                        'links': ['カテゴリ:Japanese_Teas'],
                        'images': ['tea_TEA_tea!!!!!.aPnG'],
                    },
                ],
            ),
            (
                'arywiki',
                999,
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                [
                    {'index': 0, 'heading': '### zero ###', 'links': [], 'images': []},
                    {
                        'index': 1,
                        'heading': '1',
                        'links': ['ﺖﺼﻨﻴﻓ:Moroccan_Dishes'],
                        'images': [],
                    },
                ],
            ),
        ],
        schema,
    )


@pytest.fixture(scope='session')
def images(spark_session):
    schema = T.StructType(
        [
            T.StructField('item_id', T.StringType(), False),
            T.StructField('page_id', T.IntegerType(), False),
            T.StructField('page_title', T.StringType(), False),
            T.StructField('wiki_db', T.StringType(), False),
            T.StructField(
                'article_images',
                T.ArrayType(
                    T.StructType(
                        [
                            T.StructField('index', T.IntegerType(), False),
                            T.StructField('heading', T.StringType(), False),
                            T.StructField('links', T.ArrayType(T.StringType()), False),
                            T.StructField('images', T.ArrayType(T.StringType()), False),
                        ]
                    )
                ),
            ),
        ]
    )

    return spark_session.createDataFrame(
        [
            (
                'Q666',
                666,
                'El_Diablo',
                'eswiki',
                [
                    {
                        'index': 0,
                        'heading': '### zero ###',
                        'links': ['el_día_de_la_bestia'],
                        'images': [],
                    }
                ],
            ),
            (
                'Q101',
                101,
                'Presunto',
                'ptwiki',
                [
                    {'index': 0, 'heading': '### zero ###', 'links': [], 'images': []},
                    {
                        'index': 1,
                        'heading': 'one',
                        'links': ['Ficheiro:Presunto_de_Monchique.WEBP'],
                        'images': ['Presunto_de_Monchique.WEBP'],
                    },
                ],
            ),
            (
                'Q1984',
                1984,
                '1984',
                'frwiki',
                [
                    {
                        'index': 0,
                        'heading': '### zero ###',
                        'links': ['George_Orwell'],
                        'images': [],
                    },
                    {
                        'index': 1,
                        'heading': 'Résumé',
                        'links': [],
                        'images': ['1984.tiff'],
                    },
                    {
                        'index': 2,
                        'heading': 'Personnages',
                        'links': ['Vérité'],
                        'images': [],
                    },
                ],
            ),
            (
                'Q1234',
                1234,
                '茎茶',
                'jawiki',
                [
                    {'index': 0, 'heading': '### zero ###', 'links': [], 'images': []},
                    {
                        'index': 1,
                        'heading': '1',
                        'links': ['カテゴリ:Japanese_Teas'],
                        'images': ['tea_TEA_tea!!!!!.aPnG'],
                    },
                ],
            ),
            (
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                'arywiki',
                [
                    {'index': 0, 'heading': '### zero ###', 'links': [], 'images': []},
                    {
                        'index': 1,
                        'heading': '1',
                        'links': ['ﺖﺼﻨﻴﻓ:Moroccan_Dishes'],
                        'images': [],
                    },
                ],
            ),
        ],
        schema,
    )


@pytest.fixture
def wikitext():
    # A medium-sized, well structured, and well linked Wikipedia page:
    # https://it.wikipedia.org/wiki/Gaznevada
    return '{{Artista musicale\n|nome = Gaznevada\n|nazione = ITA\n|genere = Punk rock\n|genere2 = post-punk\n|genere3 = New wave\n|genere4 = Italo disco\n|tipo artista = Gruppo\n|anno inizio attività = 1977\n|anno fine attività = 1988\n|note periodo attività = \n|etichetta = [[Harpo\'s Music]]<br />[[Italian Records]]<br />[[EMI]]<br />[[CBS (etichetta discografica)|CBS]]<br />[[Sony]]\n|totale album = 8\n|album studio = 6\n|album live =\n|raccolte = 2\n|immagine = gaznevada.jpg\n}}\nI \'\'\'Gaznevada\'\'\' sono stati un [[gruppo musicale]] [[Bologna|bolognese]] attivo tra la fine degli [[anni 1970|anni settanta]] e la fine degli [[anni 1980|anni ottanta]]. Come per altri gruppi della scena bolognese, [[Skiantos]], [[Hi-Fi Bros]] e [[Confusional Quartet]], il gruppo si formò nel fermento culturale del [[Movimento del \'77]] che in quella città era particolarmente influente. Il loro sound era inizialmente una miscela di [[Punk rock]], [[No wave]] e [[New wave (musica)|New Wave]], e nella seconda metà della loro carriera si mossero sempre più verso sonorità pop e commerciali, sviluppando una particolare forma di [[italo disco]]<ref name=NonDisp>{{cita libro|autore=|curatore=Oderso Rubini|curatore2=Andrea Tinti|titolo=Non disperdetevi. 1977-1982 San Francisco, New York, Bologna. Le città libere del mondo|editore=[[Shake Edizioni]]|città=Milano|anno=2009|ISBN=978-88-88865-89-8}}</ref>.\n\n== Storia ==\n=== 1977-1979: Dalla nascita al Bologna Rock ===\nIl gruppo si forma a [[Bologna]] verso la fine del [[1977]] nell\'ambito della scena musicale locale e come evoluzione del Centro d\'Urlo Metropolitano, formazione rock [[Movimento del \'77|movimentista]] autrice di un unico brano "Mamma dammi la benza" (album: "Sarabanda" etichetta: "Humpty Dumpty" produzione: [[Radio Alice]])<ref>{{cita web|url=https://www.radioalice.org/index.php?option=com_content&view=article&id=7:la-sarabanda&catid=2&Itemid=109|titolo=Suoni - La Sarabanda|sito=RadioAlice.org|accesso=29 gennaio 2020}}</ref>, registrato in Fonoprint e pubblicato su cassetta in occasione del [[Movimento del \'77#Convegno di Bologna|Convegno sulla Repressione]] del settembre 1977. Nel corso di questa manifestazione si esibiscono dal vivo con il brano “Mamma dammi la benza” dando vita a quello che verrà definito il “[[Rock demenziale]]”  e che vedrà negli [[Skiantos]] i maggiori esponenti del genere.\n\nIl loro ritrovo è la casa occupata, in Via Clavature 20, al primo piano, a Bologna denominata "Traumfabrik", nome ideato dal fumettista Filippo Scozzari che diviene ben presto una sorta di [[The Factory|Factory]]   frequentata da fumettisti, disegnatori, scrittori, musicisti, fotografi e video maker, nonché sede del fan-club della band.<ref name="NonDisp" />.\n\nDue mesi dopo l\'esibizione al Convegno sulla Repressione, nel novembre del 1977, di ritorno da un viaggio a [[Londra]] di alcuni componenti della band,  il gruppo decide di cambiare il nome in  "Gaznevada", ispirandosi ad un racconto di [[Raymond Chandler]] dal titolo Nevada Gas. La formazione originaria era costituita da sette elementi: Alessandro Raffini (Billy Blade), Ciro Pagano (Robert Squibb), Giorgio Lavagna (Andy Droid, successivamente Andrew Nevada), Marco Dondini (Bat Matic), Gianpietro Huber (Johnny Tramonta) e Gianluca Galliani (Nico Gamma). Nei primi anni frequenti sono i fraintendimenti ideologici da parte della sinistra italiana, che non vedeva di buon occhio l\'ondata [[Punk (cultura)|punk]] che stava investendo l\'Italia di allora e i Gaznevada che con spirito [[Situazionismo|situazionista]] ostentavano vestiti neri, spille e catene, non ne furono immuni.<ref name="Testani">{{cita libro|titolo=Enciclopedia del rock italiano|editore=[[Arcana Edizioni]]|curatore=Gianluca Testani|anno=2006|p=74|ISBN=978-8-87-966422-6}}</ref>.\n\nÈ di questo periodo, marzo 1978, il concerto “Gaznevada sing [[Ramones]]”, svoltosi per tre serate consecutive al Punkreas di Bologna, uno scantinato nel centro storico della città, nel quale la band si esibì suonando per ore solo “cover” dei brani dei Ramones con brevi pause tra un pezzo e l’altro come sui dischi originali, senza sosta e a ritmo velocizzato rispetto ai brani della band americana.<ref name="NonDisp" />. Fu proprio in quell’occasione che furono avvicinati dal neo-produttore [[Oderso Rubini]] che propose loro di registrare una serie di brani inediti, alcuni già eseguiti dal vivo, e che furono registrati negli studi, approntati in un garage della centrale via San Felice a Bologna, della neonata [[Harpo\'s Bazar|Harpo’s Bazar]], poi [[Italian Records]]. Nel marzo del 1979 viene pubblicato il primo album dei Gaznevada su supporto cassetta k7 e dal titolo “Gaznevada”. Da li a breve Gianluca Galliani abbandona la band.\n\nIl 2 aprile [[1979]] i Gaznevada partecipano al \'\'[[Bologna Rock]]\'\', un festival che si svolse al [[PalaDozza|palasport di Bologna]] e che vide sul palco i migliori gruppi dell\'allora scena [[punk rock]] e [[New wave (musica)|new wave]] bolognese. Fra questi vi erano Windopen, [[Luti Chroma]], [[Skiantos]], Bieki, Naphta, [[Confusional Quartet]], [[Andy J. Forest]], Frigos e Cheaters\n\nNella primavera del [[1979]], anche Gianpietro Huber abbandona il gruppo e viene sostituito al basso da Marco Bongiovanni (Chainsaw Sally, in seguito Marco Nevada)\n\nCon l’arrivo del nuovo bassista i Gaznevada tornano in studio per registrare, nell’autunno del 1979, il loro primo singolo: Nevadagaz contenente nel lato B l’inedito presentato al Bologna Rock: Blue TV set\n\n=== 1980-1981: Sick Soundtrack e Dressed to Kill ===\nIl secondo album "[[Sick Soundtrack]]" ([[1980]]), anticipato dal [[Singolo discografico|singolo]] "Nevada Gaz/Blue TV Set", li impone all\'attenzione della critica come una band innovativa, capace di accogliere e rielaborare le nuove sonorità in arrivo dagli [[Stati Uniti d\'America|Stati Uniti]] e dal [[Regno Unito]]<ref>{{Cita web|url=https://www.ondarock.it/pietremiliari_ita/gaznevada-sicksoundtrack.htm|titolo=Gaznevada - Sick Soundtrack :: Le Pietre Miliari di OndaRock|autore=Massimiliano Speri|sito=OndaRock.it|data=30 giugno 2019|lingua=it|accesso=29 gennaio 2020}}</ref>. Lo stile del disco era fatto di suoni nevrotici, [[sax]] molto ruvidi, ritmi quasi [[funk]], influenze [[New wave (musica)|new wave]] ([[Talking Heads]]) e [[no wave]]<ref>{{cita web|url=https://www.discogs.com/it/artist/88727-James-Chance-The-Contortions|titolo=James Chance & The Contortions <nowiki>|</nowiki> Discografia|sito=Discogs.com|accesso=29 gennaio 2020}}</ref>, ma anche [[Musica elettronica|elettroniche]], influenze rielaborate e fatte proprie in modo del tutto originale. La loro immagine [[noir]]/fantascientifica accresce la curiosità e il successo in alcuni ambienti della critica musicale dell\'epoca e nella audience di nicchia legata alla new wave e al punk. Nel 2012 l\'album \'\'Sick Soundtrack\'\' verrà inserito da [[Rolling Stone#Rolling Stone in Italia|Rolling Stone Italia]] tra [[I 100 migliori album italiani secondo Rolling Stone#Classifica|i migliori 100 dischi di rock italiano]]<ref>{{cita web|url=https://httpraulrizzardiwordpresscom.wordpress.com/2019/03/15/lista-dei-100-migliori-album-italiani-secondo-rolling-stone/amp/|titolo=Lista dei 100 migliori album italiani secondo Rolling Stone|autore=Raul Rizzardi RitZ\'Ó|sito=httpRaulRizzardiWordpresscom.Wordpress.com|data=febbraio 2019|accesso=29 gennaio 2020}}</ref><ref>{{cita web|url=https://www.lamusicarock.com/cantanti-italiani/i-100-dischi-italiani-piu-belli-di-sempre-secondo-rolling-stone/|titolo=I 100 dischi italiani più belli di sempre secondo Rolling Stone|sito=LaMusicaRock.com|accesso=15 gennaio 2019|urlarchivio=https://web.archive.org/web/20181212112452/https://www.lamusicarock.com/cantanti-italiani/i-100-dischi-italiani-piu-belli-di-sempre-secondo-rolling-stone/|dataarchivio=12 dicembre 2018|urlmorto=sì}}</ref>. Alle prime mille copie del [[Long playing|33 giri]] era allegato un singolo 7" inciso su una sola facciata, con il brano "I See My Baby Standing on a Plane" eseguito dagli stessi Gaznevada con lo pseudonimo "Billy Blade and the Electric Razors", gruppo [[rockabilly]] che successivamente si esibirà in alcune occasioni dal vivo con membri dei Gaznevada e dei [[Confusional Quartet]]. Dall’album Sick Soundtrack viene anche prodotto un video per il brano “Shock Antistatico”.\n\nÈ di questo periodo la vignetta di [[Andrea Pazienza]] pubblicata sul Male che li ritrae in concerto su un asteroide piatto che fluttua nello spazio. Molti anni dopo, nel 2002, il regista [[Renato De Maria]], anch’egli frequentatore della Traumfabrik di quegli anni, realizzerà il film "[[Paz!]]", sulla vita e l’arte di Andrea Pazienza, e inserì nella colonna sonora il brano “Japanese Girls” estratto dall’album Sick Soundtrack.\n\nNel 1980 Ciro Pagano (Robert Squibb), Marco Bongiovanni (Chainsaw Sally), Marco Dondini (Bat Matic) suonano rispettivamente chitarra, basso e batteria per [[Edoardo Bennato]] nel brano "Uffà! Uffà!", all\'interno dell\'[[Uffà! Uffà!|album omonimo]]. Nello stesso anno compaiono nel film “[[Si salvi chi vuole]]” per la regia di [[Roberto Faenza]] con [[Claudia Cardinale]].\n\nNel 1981 i Gaznevada pubblicano sempre con l\'etichetta Italian Records e la produzione di Oderso Rubini il [[mini-LP]] "Dressed to Kill", che si può considerare la continuazione del percorso musicale/artistico intrapreso dalla band con "Sick Soundtrack" e allo stesso tempo la sua conclusione. Il disco, ispirato all’[[Vestito per uccidere|omonimo film]] di [[Brian De Palma]], è un “concept” dalle influenze no wave ed elettroniche dominanti di quel periodo. L’intero album sarà registrato e mixato a Bologna negli studi della Fonoprint.\n\nNell’estate del 1981 i Gaznevada partecipano, con vestiti e trucco ispirati ai [[samurai]] giapponesi, al festival "Electra 1. Festival per i Fantasmi del Futuro", che si svolse a Bologna tra il 17 e il 21 luglio e al quale, oltre ai Gaznevada parteciparono diversi artisti di spicco della scena musicale new wave internazionale: [[Bauhaus (gruppo musicale)|Bauhaus]], [[DNA (gruppo musicale)|DNA]], [[Brian Eno]], [[Peter Gordon]], [[Chrome (gruppo musicale)|Chrome]], [[The Lounge Lizards]] e gli italiani: [[N.O.I.A.]], [[Rats]], [[Band-Aid]] oltre al gruppo teatrale [[Magazzini Criminali]].\n\nNel 1982 Giorgio Lavagna (aka Andy Nevada) lascia la band per proseguire la sua esperienza musicale con il gruppo elettronico “[[The Stupid Set]]” dei quali fu fondatore e i Gaznevada iniziano un percorso sonoro diverso, che rompe in qualche modo gli schemi con il proprio passato. Escono contemporaneamente il singolo 7” “Ragazzi dello Spazio” e il Mix 12” “(Black Dressed) White Wild Boys” quest’ultimo dalle sonorità funky ed entrambi cantati in italiano. Successivamente partecipano con il brano inedito “Detectives” che interpreteranno anche nel ruolo di attori, al musical cinematografico “Pirata!”, per la regia di Paolo Ricagno e presentato l\'anno successivo alla [[Biennale di Venezia]].\n\n=== 1983: Svolta pop e italo disco ===\nLa vera svolta musicale avviene nel 1983 con il brano “I.C. Love Affair” contenuto nell’album “Psicopatico Party”. Il brano, ad oggi considerato una “cult track” della musica clubbing, e suonata da numerosi dj di levatura internazionale, ottiene un notevole successo e proietta il gruppo nelle classifiche di vendita e di gradimento. Nel frattempo, poco dopo l’uscita dell’album, anche Marco Dondini (Bat Matic) abbandonerà il gruppo sostituito da Gianni Cuoghi (ex batterista dei Confusional Quartet).\n\nGrazie al successo di “I.C. Love Affair” firmano un nuovo contratto con la casa discografica [[EMI]] e partecipano ospiti a numerosi programmi televisivi tra cui il [[Festivalbar]]. Successivamente usciranno altri due singoli “Special Agent Man” e nel 1984 “Ticket To Los Angeles” prima di realizzare nel 1985 “Back To The Jungle” il primo album per la EMI. L’album conteneva il brano “Living in The Jungle” che fu pubblicato come singolo ed ebbe un discreto successo di vendita e di pubblico. Dal singolo venne prodotto un video per conto della [[Rai|RAI]] che divenne sigla del programma per ragazzi “[[Cartoni magici|Cartoni Magici]]” in onda tutti i giorni su [[Rai 1|RAI1]]. La regia era di [[Renzo Martinelli]] e gli inserti di animazione curati da [[Bruno Bozzetto]]. “Living in the Jungle” arrivò al 1º posto nella categoria ‘Video-clip’ del mensile ‘[[Musica e dischi|Musica E Dischi]]’ e venne premiato per la sua realizzazione e gli effetti speciali. Dall’album “Back to the Jungle” sarà estratto anche il singolo “Mary is a Clerk”. Lo stesso anno compaiono nel film “Dolce Assenza” di Claudio Sestieri con [[Jo Champa]] e [[Sergio Castellitto]]. Per la colonna sonora del film realizzano anche una versione ad hoc del brano “Railway Station Boy” contenuto nell’album “Back To The Jungle”. L’anno successivo nel 1986 viene realizzato il singolo “Sex Sister”  primo ed unico disco interamente prodotto dai Gaznevada.\n\nNel 1987 con l’uscita di Gianni Cuoghi e di Alessandro Raffini (Billy Blade) e l’ingresso, alla voce, di Nicola Guiducci, dj e fondatore del [[Plastic (locale)|Plastic]], storico locale di Milano, la formazione subisce un’ultima e definitiva variazione di organico e della band originale restano solo Ciro Pagano e Marco Bongiovanni. Lo stesso anno i Gaznevada entrano nuovamente in studio e realizzano un nuovo album per conto della [[CBS Italiana|CBS]] e prodotto da [[Guido Elmi]], storico produttore di [[Vasco Rossi]].\n\nAl termine del 1987 viene pubblicato il singolo “Thrill Of The Night” contenuto nell\'album “Strange Life” il quarto ed ultimo album della band in uscita nel 1988 e da cui verranno estratti altri due singoli, “Sometimes” e "Jimmy Boy".\n\n=== Dopo i Gaznevada ===\nTerminata l’esperienza Gaznevada, nel 1988, Ciro Pagano si dedicherà a diverse produzioni nell’ambito della dance e successivamente darà vita al gruppo [[techno]] [[Datura (gruppo musicale)|Datura]] mentre Marco Bongiovanni produrrà, tra gli altri, il progetto [[italo house]] ” DJ H feat.Stefy" Successivamente, dopo anni di silenzio, nel 2002 esce per la EMI Dance Factory un nuovo inedito dei Gaznevada: il singolo di matrice elettronica “Dance no Dance”. La produzione artistica è dei Datura mentre la copertina, che richiama la grafica di Sick Soundtrack, è curata da Giorgio Lavagna, primo cantante del gruppo.\n\nNel 2020 a quarant’anni dalla prima pubblicazione viene ristampato, su etichetta Italian Records, e per conto di Expanded Music,  l\'album “Sick Soundtrack” in versione limitata e remasterizzata. Da li a breve viene anche pubblicata su vinile la cassetta "Gaznevada", primo lavoro della band.\n\n==Formazione==\n===Formazione iniziale (1979)===\n* Alessandro Raffini (Sandy Banana / Billy Blade) - [[canto|voce]], [[sassofono]], [[organo (strumento musicale)|organo elettrico]]\n* Gianpietro Huber (Johnny Tramonta / Hal Capra) - [[basso elettrico|basso]]\n* Marco Dondini (Bat Matic) - [[Batteria (strumento musicale)|batteria]]\n* Giorgio Lavagna (Andy Droid / Andrew Nevada) - voce, [[Sintetizzatore|synth]]\n* Ciro Pagano (Robert Squibb) - [[chitarra elettrica|chitarra]]\n* GianLuca Galliani (Nico Gamma) - [[Tastiera elettronica|tastiere]]\n\n===In formazioni successive (1979-1988)===\n* Marco Bongiovanni (Chainsaw Sally / Marco Nevada) - basso, synth\n* Gianni Cuoghi - batteria\n* Nicola Guiducci - voce\n\n== Discografia ==\n=== Album in studio ===\n*1979 - Gaznevada ([[Harpo\'s Bazar]])\n*1980 - [[Sick Soundtrack]] ([[Italian Records]])\n*1983 - Psicopatico Party (Italian Records)\n*1985 - Back To The Jungle ([[EMI]])\n*1988 - Strange Life ([[CBS]])\n=== EP ===\n*1981 - Dressed To Kill (Italian Records, 12")\n=== Singoli ===\n*1980 - Nevadagaz / Blue TV set ([[Italian Records]], 7")\n*1982 - Ragazzi Dello Spazio / Dolly (Italian Records, 7") \n*1982 - (Black Dressed) White Wild Boys - (Italian Records, 12”)\n*1983 - I.C. Love Affair / Agente Speciale - (Italian Records 7” - 12”)\n*1983 - Special Agent Man - (Italian Records 7” - 12”)\n*1984 - Ticket To Los Angeles / Macho’s Love On The Rock - (Italian Records 7” - 12”)\n*1985 - Living In The Jungle / Western Boys, Eastern Girls - (EMI 7” - 12”)\n*1985 - Mary Is A Clerk / Western Boys, Eastern Girl - (EMI 7” - 12”)\n*1986 - Sex Sister / Change Your Old Ideas - (EMI 7” - 12”)\n*1987 - Thrill Of The Night - (CBS 7” - 12”)\n*1988 - Sometimes, Somewhere, Someone / Too Deep For Dealing - (CBS 7”)\n*1988 - Jimmy Boy / Sometimes - (CBS 12”)\n*2002 - Dance no Dance - (EMI 12” - CDs)\n\n==Note==\n<references/>\n\n==Bibliografia==\n* {{cita libro|autore=A.A.V.V.|curatore= Cesare Rizzi |titolo=Enciclopedia del rock italiano| editore= [[Arcana Editore|Arcana]] |città=Milano | anno= 1993 |cid=Rizzi |id=ISBN 8879660225}}\n* {{cita libro|autore=A.A.V.V.|titolo=Enciclopedia del rock italiano|editore=[[Arcana Editrice]]|curatore =Gianluca Testani|anno=2006|isbn=88-7966-422-0}}\n* {{cita libro|autore=Paolo Bertrando|titolo=Bologna Rock|editore=[[Edizioni Re Nudo]]|città=Padova |anno= 1980}}\n* {{cita libro|autore=Alessandro Bolli|titolo=Dizionario dei Nomi Rock|editore=[[Arcana editrice]]|città=Padova |anno= 1998|isbn=978-88-7966-172-0}}\n* {{cita libro|autore=Arturo Compagnoni|titolo=Italia \'80. Il Rock indipendente italiano negli anni Ottanta|editore=[[Apache Edizioni]]|città= |anno= 2004}}\n* {{cita libro|autore=Luca Frazzi|titolo=Le guide pratiche di RUMORE - Punk italiano parte prima. Mamma dammi la benza|editore=Apache Edizioni|città=Pavia|anno= 2003}}\n* {{cita libro|autore=[[Marco Philopat]]|titolo=Lumi di punk: la scena italiana raccontata dai protagonisti|editore=[[Agenzia X]]|città= |anno= 2006}}\n* {{cita libro|autore=|curatore= Oderso Rubini, Andrea Tinti|titolo=Non disperdetevi. 1977-1982 San Francisco, New York, Bologna. Le città libere del mondo|editore=[[Shake edizioni]]|città=Milano |anno= 2009|isbn=978-88-88865-89-8}}\n* [[Giuseppe Sterparelli]] (a cura di), \'\'I maledetti del rock italiano, segni e suoni di strada da Clem Sacco ai 99 Posse\'\' (edizioni Del Grifo), con 45 tavole originali dedicate ai rinnovatori della scena musicale nazionale e saggi di [[Vincenzo Sparagna]], [[Luca Frazzi]] (\'\'[[Rumore (rivista)|Rumore]]\'\'), [[Freak Antoni]]\n* Oderso Rubini, GianLuca Galliani \'\'Mamma dammi la benza! Il primo disco (punk) con estratto libro di GianLuca Galliani "Gaznevada History & Hysteria" dei Gaznevada!\'\', [[Shake Edizioni]], Milano 2009. ISBN 978-88-88865-64-5\n* {{cita libro|autore=Enrico Scuro|curatore= Marzia Bisognin, Paolo Ricci|titolo=I Ragazzi del \'77\' Una storia condivisa su Facebook|editore=[[Baskerville (casa editrice)|Baskerville]]|città=Bologna |anno= 2011|isbn=978-88-8000-892-7}}\n* {{cita libro|autore=Diego Nozza|titolo=Hardcore. Introduzione al punk italiano degli anni ottanta|editore=[[Crac edizioni]]|città=Fano |anno= 2011|isbn=978-88-97389-02-6}}\n* Emanuele Angiuli, Imago Orbis \'\'Traumfabrik. Via Clavature, 20\'\', DVD e Libro (Angiuli, Huber, Scozzari, Galliani, Lavagna, Sabbioni, De Maria, Dondini, Raffini e F. Bifo Berardi. Produzione [[Imago Orbis]]\n* {{cita libro|autore=Livia Satriano|titolo=No Wave. Contorsionismi e sperimentazioni dal CBGB\'s al Tenax|editore=Crac edizioni|città=Fano |anno= 2012|isbn=978-88-97389-04-0}}\n* Livia Satriano, \'\'Gli altri Ottanta. Racconti dalla galassia post-punk italiana\'\'. Agenzia X, 2014. ISBN 978-88-95029-99-3\n\n==Collegamenti esterni==\n* {{Collegamenti esterni}}\n* [http://www.astroman.it/astro/astro_artist.asp?cat_id=1&artist_id=1 Gaznevada] in astroman.it\n\n{{Controllo di autorità}}\n{{Portale|Punk}}\n\n[[Categoria:Gruppi e musicisti dell\'Emilia-Romagna]]\n[[Categoria:Gruppi musicali new wave italiani]]'
