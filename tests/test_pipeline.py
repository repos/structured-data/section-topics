#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

from math import log
from typing import List

import pytest

from chispa.column_comparer import assert_column_equality
from chispa.dataframe_comparer import assert_approx_df_equality, assert_df_equality

from section_topics import pipeline


# This class targets `pipeline._grab_images`
class TestGrabImages:
    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                '[[File:A_portrait_of_A,_by_B–C.PNG|thumb|center|x60px|alt=Foo bar baz|Example image]]',
                ['A_portrait_of_A,_by_B–C.PNG'],
            ),
            (
                "[[File:Example - ita, 1825 - 766672 R (cropped).jpeg|thumb|left|The ''[[example]]]'' image.]]",
                ['Example_-_ita,_1825_-_766672_R_(cropped).jpeg'],
            ),
        ],
    )
    def test_image(self, wikitext: str, expected: List[str]) -> None:
        assert pipeline._grab_images(wikitext) == expected

    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                """{{Infobox book
                    | name           = Anne of Green Gables
                    | image          = FirstPageGreenGables.gif
                    | caption        = The front (first) page of the first edition
                    | website        = https://www.anneofgreengables.com/
                    }}""",
                ['FirstPageGreenGables.gif'],
            ),
            (
                """{{Infobox album
                    | name       = Modal Soul
                    | type       = studio
                    | artist     = [[Nujabes]]
                    | cover      = ModalMusic.jpg
                    }}""",
                ['ModalMusic.jpg'],
            ),
        ],
    )
    def test_infobox(self, wikitext: str, expected: List[str]) -> None:
        assert pipeline._grab_images(wikitext) == expected

    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                """<gallery widths="170" heights="170">
                MET 1984 482 237872.jpg|Seal; 3000–1500 BC; baked [[steatite]]; 2 × 2&nbsp;cm; [[Metropolitan Museum of Art]] (New York City)
                File:Stamp seal and modern impression- unicorn and incense burner (?) MET DP23101 (cropped).jpg|Stamp seal and modern impression: unicorn and incense burner (?); 2600-1900&nbsp;BC; burnt steatite; 3.8 × 3.8 × 1&nbsp;cm; Metropolitan Museum of Art
                Clevelandart 1973.160.jpg|Seal with two-horned bull and inscription; 2010 BC; steatite; overall: 3.2 x 3.2&nbsp;cm; [[Cleveland Museum of Art]] ([[Cleveland]], [[Ohio]], US)
                </gallery>""",
                [
                    'MET_1984_482_237872.jpg',
                    'Stamp_seal_and_modern_impression-_unicorn_and_incense_burner_(?)_MET_DP23101_(cropped).jpg',
                    'Clevelandart_1973.160.jpg',
                ],
            ),
            (
                """{{Gallery
                   |title=Example gallery
                   |width=160 | height=170
                   |align=center
                   |footer=Example 1
                   |File:An Image taken between 1950-1960.JPG
                    |An example [[image]] taken in the [[1950s]]
                    |alt1=Example alt text for an example image
                   }}""",
                ['An_Image_taken_between_1950-1960.JPG'],
            ),
        ],
    )
    def test_unnamed_parameter(self, wikitext: str, expected: List[str]) -> None:
        assert pipeline._grab_images(wikitext) == expected

    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                # https://phabricator.wikimedia.org/T335180
                '{{wide image|Schönbrunn, Viedeň, Rakúsko.jpg|800px|[[Schönbrunn Palace]]}}',
                ['Schönbrunn,_Viedeň,_Rakúsko.jpg'],
            ),
            (
                '{{wide image|other param|Schönbrunn, Viedeň, Rakúsko.jpg|800px|[[Schönbrunn Palace]]}}',
                ['Schönbrunn,_Viedeň,_Rakúsko.jpg'],
            ),
        ],
    )
    def test_gallery(self, wikitext: str, expected: List[str]) -> None:
        assert pipeline._grab_images(wikitext) == expected

    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                '[[A_portrait_of_A,_by_B–C.PNG|thumb|center|x60px|alt=Foo bar baz|Example image]]',
                [],
            ),
            (
                "[[  Example - ita, 1825 - 766672 R (cropped).jpeg|thumb|left|The ''[[example]]]'' image.]]",
                [],
            ),
        ],
    )
    def test_invalid_prefix(self, wikitext: str, expected: List[str]) -> None:
        assert pipeline._grab_images(wikitext) == expected


def test_page_filter(spark_session, pages):
    # Test `apply_filter` with pages

    with pytest.raises(ValueError):
        bad_columns = spark_session.createDataFrame([(4, 3), (2, 1)], ['no', 'good'])
        pipeline.apply_filter(pages, bad_columns)
    bad_pages = spark_session.createDataFrame(
        [
            ('eswiki', 77),  # Not in `pages`
            ('frwiki', 98),  # To be filtered
            ('ptwiki', 1984),  # Not in `pages`
        ],
        ['wiki_db', 'revision_id'],
    )
    expected = spark_session.createDataFrame(
        [
            ('eswiki', 42, 666, 'El_Diablo', '#REDIRECCIÓN [[El día de la bestia]]'),
            ('itwiki', 69, 127, 'Fiat_127', 'La Fiat 127 è una [[autovettura]]'),
        ],
        ['wiki_db', 'revision_id', 'page_id', 'page_title', 'revision_text'],
    )
    actual = pipeline.apply_filter(pages, bad_pages)

    assert_df_equality(expected, actual, ignore_column_order=True)


def test_date_qid_filter(spark_session, topics_with_dates):
    # Test `apply_filter` with date QIDs.
    # See `scripts/fetch_qids_from_wikidata.py` and `scripts/sparql/qids_for_all_points_in_time.sparql`.

    with pytest.raises(ValueError):
        bad_columns = spark_session.createDataFrame([('Q666',), ('Q610',)], ['datez'])
        pipeline.apply_filter(topics_with_dates, bad_columns)
    date_qids = spark_session.createDataFrame(
        [
            ('Q2550',),
            ('Q2503',),
            ('Q2961',),
            ('Q2986',),
            ('Q2920',),
            ('Q2247',),
            ('Q2221',),
            ('Q2255',),
            # Not in `topics_with_dates`
            ('Q69166656',),
            ('Q69135811',),
            ('Q69190802',),
            ('Q69105814',),
            ('Q13470307',),
            ('Q69256113',),
            ('Q69317134',),
            ('Q69309663',),
        ],
        ['topic_qid'],
    )
    expected = spark_session.createDataFrame(
        [
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                2,
                'Q745758',
                'Глостер_(регбийный_клуб)',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                3,
                'Q1149835',
                'Харлекуинс',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                4,
                'Q1201283',
                'Нортгемптон_Сэйнтс',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                5,
                'Q1548720',
                'Ольстер_(регбийный_клуб)',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                7,
                'Q1471265',
                'Перпиньян_(регбийный_клуб)',
            ),
            (
                'ruwiki',
                1,
                'Q1',
                1,
                'Мюррей,_Коннор',
                1,
                'Клубная_карьера',
                9,
                'Q297901',
                'Клермон_Овернь',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'section_id',
            'topic_qid',
            'topic_title',
        ],
    )
    actual = pipeline.apply_filter(topics_with_dates, date_qids, broadcast=True)

    assert_df_equality(expected, actual, ignore_column_order=True, ignore_nullable=True)


def test_media_qid_filter(spark_session, topics_with_media):
    # Test `apply_filter` with media QIDs.
    # See `scripts/sparql/qids_for_all_points_in_time.sparql` and `scripts/sparql/qids_for_media_outlets.sparql`.

    with pytest.raises(ValueError):
        bad_columns = spark_session.createDataFrame(
            [(16, 11, 'Q1984'), (20, 2, 'Q2002')], ['I', 'M', 'media']
        )
        pipeline.apply_filter(topics_with_media, bad_columns)
    media_qids = spark_session.createDataFrame(
        [
            ('Q40469',),
            ('Q1160945',),
            ('Q40464',),
            ('Q166180',),
            ('Q130879',),
            ('Q1665444',),
            # Not in `topics_with_media`
            ('Q43508725',),
            ('Q114532322',),
            ('Q71778809',),
            ('Q111800271',),
            ('Q11225072',),
            ('Q902550',),
            ('Q17962520',),
            ('Q85554286',),
        ],
        ['topic_qid'],
    )
    expected = spark_session.createDataFrame(
        [
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q90',
                'Paris',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q6891134',
                'Mohamed_Djaanfari',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q1041',
                'Senegal',
            ),
            (
                'enwiki',
                1,
                'Q660843',
                1,
                '2008_invasion_of_Anjouan',
                1,
                'aftermath',
                1,
                'Q158692',
                'Comorian_Armed_Forces',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'section_id',
            'topic_qid',
            'topic_title',
        ],
    )
    actual = pipeline.apply_filter(topics_with_media, media_qids, broadcast=True)

    assert_df_equality(expected, actual, ignore_column_order=True, ignore_nullable=True)


def test_table_filter(spark_session, sections):
    # Test `apply_filter` with sections holding HTML tables

    with pytest.raises(ValueError):
        bad_columns = spark_session.createDataFrame(
            [(6, 1, 'zeroooo!'), (6, 6, 'six')], ['u', 'r', 'table']
        )
        pipeline.apply_filter(sections, bad_columns)
    tables = spark_session.createDataFrame(
        [
            ('frwiki', 1984, 'Résumé'),
            ('frwiki', 1984, 'Personnages'),
            ('idwiki', 1411667, 'Babak_gugur'),  # Not in `sections`
            ('jawiki', 1234, '1'),
            ('arywiki', 4321, '1'),
            ('ptwiki', 5322869, 'Calendário'),  # Not in `sections`
        ],
        ['wiki_db', 'page_id', 'section_title'],
    )
    expected = spark_session.createDataFrame(
        [
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                0,
                '### zero ###',
                'el_día_de_la_bestia',
                'el_día_de_la_bestia',
            ),
            ('ptwiki', 37, 'Q101', 101, 'Presunto', 0, '### zero ###', None, None),
            (
                'ptwiki',
                37,
                'Q101',
                101,
                'Presunto',
                1,
                'one',
                'Ficheiro:Presunto_de_Monchique.WEBP',
                'Ficheiro:presunto_de_Monchique.WEBP',
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                0,
                '### zero ###',
                'George_Orwell',
                'george_Orwell',
            ),
            ('jawiki', 610, 'Q1234', 1234, '茎茶', 0, '### zero ###', None, None),
            ('arywiki', 999, 'Q4321', 4321, 'ﻁﺎﺠﻴﻧ', 0, '### zero ###', None, None),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'link',
            'normalized_link',
        ],
    )
    actual = pipeline.apply_filter(sections, tables)

    assert_df_equality(
        expected, actual, ignore_row_order=True, ignore_column_order=True
    )


def test_look_up_qids(spark_session, pages):
    qids = spark_session.createDataFrame(
        [
            ('eswiki', 'Q666', 666),
            ('ptwiki', 'Q610', 4321),
            (
                'dewiki',
                'Q1407',
                1407,
            ),  # Same page ID, different wiki: shouldn't be there
        ],
        ['wiki_db', 'item_id', 'page_id'],
    )
    expected = spark_session.createDataFrame(
        [
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                '#REDIRECCIÓN [[El día de la bestia]]',
            )
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'revision_text',
        ],
    )
    actual = pipeline.look_up_qids(pages, qids)

    assert_df_equality(expected, actual, ignore_schema=True)


def test_wikitext_headings_to_anchors():
    headings = [
        '== [[@&one! ($$band)#%]] ==',
        '== <^\u200a|two~\u3000>?+ ==',
        '== [[]] ==',
        '== [[text]] ==',
        '== [] ==',
        '== [//en.wikipedia.org text] ==',
        '== [https://en.wikipedia.org text] ==',
        '== [ftp://en.wikipedia.org text] ==',
        '== [en.wikipedia.org text] ==',
        '== <a>abc</a> ==',
        '== <strong>abc</strong> ==',
        '== abc<br>def ==',
        '== <li>abc</li> ==',
        '== <sub>abc</sub> ==',
        '== <tr>abc</tr> ==',
        '== <dd>abc</dd> ==',
        '== <table>abc</table> ==',
        '== <div>abc</div> ==',
        '== <pre>abc</pre> ==',
        '== <img> ==',
        '== <h6>abc</h6> ==',
        # https://test.wikipedia.org/wiki/Section_links
        '== Welcome! ==',
        '== Links to sections on other pages ==',
        '== Links to sections on this page ==',
        '==  Section  with  extra  spaces ==',
        '== Funny section !@#$%^&*() ==',
        '== !@#$%^&*() ==',
        '== Links to sections on other pages ==',
        '== Links to sections on this page ==',
        '==  Section  with  extra  spaces ==',
        '== Welcome! ==',
        '== Links to sections on other pages ==',
        '== Links to sections on this page ==',
        '==  Section  with  extra  spaces ==',
        '== Funny section !@#$%^&*() ==',
        '== !@#$%^&*() ==',
        '== Links to sections on other pages ==',
        '== Links to sections on this page ==',
        '==  Section  with  extra  spaces ==',
        '== Funny section !@#$%^&*() ==',
        '== !@#$%^&*() ==',
        # https://en.wikipedia.beta.wmflabs.org/w/index.php?title=Talk:Test_ascii_toc
        '==Testing · dot==',
        '==  Test accented characters périmètre ==',
        '== 存在 ==',
        '== `backtick test ==',
        '== "Quote" test ==',
        '== 2 > 1 ==',
        '== 😭 ==',
        '== abc123 ==',
        '== "YOLO+2!" ==',
        "== 😭abc123!*'();:@&=+$,/?#[]%F0%9F%98%AD存在%😭 ==",
        '== this is a title with <b>bold</b> text but it should not appear in overlay title ==',
    ]
    expected = [
        '@&one!_($$band)#%',
        '<^\u200a|two~\u3000>?+',
        '[[]]',
        'text',
        '[]',
        'text_2',
        'text_3',
        'text_4',
        '[en.wikipedia.org_text]',
        '<a>abc</a>',
        'abc',
        'abcdef',
        'abc_2',
        'abc_3',
        'abc_4',
        'abc_5',
        'abc_6',
        'abc_7',
        'abc_8',
        '<img>',
        'abc_9',
        'Welcome!',
        'Links_to_sections_on_other_pages',
        'Links_to_sections_on_this_page',
        'Section_with_extra_spaces',
        'Funny_section_!@#$%^&*()',
        '!@#$%^&*()',
        'Links_to_sections_on_other_pages_2',
        'Links_to_sections_on_this_page_2',
        'Section_with_extra_spaces_2',
        'Welcome!_2',
        'Links_to_sections_on_other_pages_3',
        'Links_to_sections_on_this_page_3',
        'Section_with_extra_spaces_3',
        'Funny_section_!@#$%^&*()_2',
        '!@#$%^&*()_2',
        'Links_to_sections_on_other_pages_4',
        'Links_to_sections_on_this_page_4',
        'Section_with_extra_spaces_4',
        'Funny_section_!@#$%^&*()_3',
        '!@#$%^&*()_3',
        'Testing_·_dot',
        'Test_accented_characters_périmètre',
        '存在',
        '`backtick_test',
        '"Quote"_test',
        '2_>_1',
        '😭',
        'abc123',
        '"YOLO+2!"',
        "😭abc123!*'();:@&=+$,/?#[]%F0%9F%98%AD存在%😭",
        'this_is_a_title_with_bold_text_but_it_should_not_appear_in_overlay_title',
    ]
    actual = pipeline.wikitext_headings_to_anchors(headings)

    assert expected == actual


def test_normalize_heading_column(spark_session):
    ddf = spark_session.createDataFrame(
        [
            ('== This is a section topic ==', 'this is a section topic'),
            ('== This_is_a_section_topic ==', 'this is a section topic'),
            ('This is a section topic', 'this is a section topic'),
            ('This_is_a_section_topic', 'this is a section topic'),
            ('This_is_a_section_topic_2', 'this is a section topic'),
        ],
        ['heading', 'expected'],
    )
    # Apply the UDF
    ddf = ddf.withColumn('actual', pipeline.normalize_heading_column('heading'))

    assert_column_equality(ddf, 'expected', 'actual')


def test__is_content_longer_or_equal_than():
    assert pipeline._is_content_longer_or_equal_than(None, 1) is False
    assert pipeline._is_content_longer_or_equal_than('', 1) is False
    assert pipeline._is_content_longer_or_equal_than('a', 1) is True
    assert pipeline._is_content_longer_or_equal_than('av', 1) is True
    assert pipeline._is_content_longer_or_equal_than('av', -1) is True


def test_parse_excluding_with_minimum_section_size(spark_session, wikitext):
    # Test the currying function

    # - Total sections including lead = 7 (<lead>, Storia, Formazione, Discografia, Note, Bibliografia, Collegamenti esterni)
    # - sections shorter than 100 chars = 2
    # - expected = 7 - 2 = 5
    expected_total_sections = 5
    container = spark_session.createDataFrame([(wikitext,)], ['raw'])
    # Keep lists and tables
    container = container.withColumn(
        'parsed', pipeline.parse_wikitext(True, 100)('raw')
    )
    actual = container.collect()

    # Test section exclusion
    actual_wikitext = actual[0]['parsed']
    assert expected_total_sections == len(actual_wikitext)


def test_parse_excluding_with_ref_tags(spark_session, wikitext):
    # Test the currying function

    # add ref tags to 2 of the section headers
    wikitext = re.sub(
        r'=== Dopo i Gaznevada ===',
        '=== Dopo i Gaznevada <ref something /> ===',
        wikitext,
    )
    wikitext = re.sub(
        r'=== 1983: Svolta pop e italo disco ===',
        '=== 1983: Svolta pop e italo disco <ref>something</ref> ===',
        wikitext,
    )

    # - Total sections including lead = 7 (<lead>, Storia, Formazione, Discografia, Note, Bibliografia, Collegamenti esterni)
    # - sections with ref tags in the header = 2 (these will be skipped)
    # - expected = 7 - 2 = 5
    expected_total_sections = 5
    container = spark_session.createDataFrame([(wikitext,)], ['raw'])
    # Keep lists and tables
    container = container.withColumn(
        'parsed', pipeline.parse_wikitext(True, 100)('raw')
    )
    actual = container.collect()

    # Test section exclusion
    actual_wikitext = actual[0]['parsed']
    assert expected_total_sections == len(actual_wikitext)


def test_parse(spark_session, wikitext, lead_section):
    # Test the UDF

    # `wikitext` is https://it.wikipedia.org/wiki/Gaznevada:
    # - total sections including lead = 7 (<lead>, Storia, Formazione, Discografia, Note, Bibliografia, Collegamenti esterni)
    # - sections with lists = 4
    # - expected = 7 - 4 = 3
    expected_total_sections = 3
    # Normalization replaces every non-standard whitespace with '_' and strips !"#$%&\' *+,-./:;<=>?@[\\]^_`{|}~
    weird_titles = (
        'zero\n\n\n==[[@&one! ($$band)#%]]==\n\n===sub===\n==<^\u200a|two~\u3000>?+==\n'
    )
    expected_titles = ('@&one!_($$band)#%', '<^\u200a|two~\u3000>?+')
    # Dataframe that holds the `wikitext` fixture and `weird_titles`
    container = spark_session.createDataFrame(
        [
            (
                'itwiki',
                wikitext,
            ),
            (
                'itwiki',
                weird_titles,
            ),
        ],
        ['wiki', 'raw'],
    )
    # Apply the UDF: no denylist, skip lists and tables
    container = container.withColumn('parsed', pipeline.parse_wikitext(False, 0)('raw'))
    # Take the UDF output
    actual = container.collect()

    # Test section extraction & lists/tables filter
    actual_wikitext = actual[0]['parsed']
    assert expected_total_sections == len(actual_wikitext)
    # Test final output
    assert lead_section == actual_wikitext[0].asDict()
    # Test section title normalization
    actual_weird_titles = actual[1]['parsed']
    assert expected_titles == (
        actual_weird_titles[1].asDict()['heading'],
        actual_weird_titles[2].asDict()['heading'],
    )


def test_extract_sections(spark_session, pages_with_qids, sections, images):
    # No link normalization happens here, so drop it from the ``sections`` fixture
    expected_links = sections.drop('normalized_link')
    actual_links, actual_images = pipeline.extract_sections(pages_with_qids, False, 0)

    assert_df_equality(expected_links, actual_links, ignore_schema=True)
    assert_df_equality(images, actual_images, ignore_schema=True)


def test_normalize_wikilinks(spark_session):
    # Inspired by https://github.com/MrPowers/chispa#column-equality
    ddf = spark_session.createDataFrame(
        [
            ('五百円硬貨', '五百円硬貨'),
            ('Западная Европа', 'западная_Европа'),
            ('كلاسيكية قديمة', 'كلاسيكية_قديمة'),
            ('Pippo', 'pippo'),
            # Corner cases
            (None, None),
            ('F', 'f'),
            # Empty strings shouldn't be passed, test robustness against them
            ('', ''),
        ],
        ['wikilink', 'expected'],
    )
    # Apply the UDF
    ddf = ddf.withColumn('actual', pipeline.normalize_wikilinks('wikilink'))

    assert_column_equality(ddf, 'expected', 'actual')


def test_handle_media(spark_session):
    # NOTE The tested function expects a full dataframe of extracted sections,
    # but only processes links, so we focus on them for simplicity
    media_prefixes = [
        '图像',
        '文件',  # Chinese
        'Fichier',
        'Image',  # French
        'Ficheiro',  # Portuguese, not in `links`
    ]
    links = spark_session.createDataFrame(
        [
            ('图像:Chinese_Media',),
            ('Fichier:A_French_File',),
            # Not in `media_prefixes`
            ('Archivo:A_Spanish_File',),
            # Regular links
            ('Eddie_Cochran',),
            ('Punkreas',),
            ('Siniestro_Total',),
            # Corner cases
            (':d:Q666',),
            ('UTC−02:00',),
        ],
        [
            'link',
        ],
    )
    expected_media = spark_session.createDataFrame(
        [
            ('图像:Chinese_Media',),
            ('Fichier:A_French_File',),
        ],
        [
            'link',
        ],
    )
    expected_remainder = spark_session.createDataFrame(
        [
            # Not in `media_prefixes`
            ('Archivo:A_Spanish_File',),
            # Regular links
            ('Eddie_Cochran',),
            ('Punkreas',),
            ('Siniestro_Total',),
            # Corner cases
            (':d:Q666',),
            ('UTC−02:00',),
        ],
        [
            'link',
        ],
    )
    actual_media, actual_remainder = pipeline.handle_media(links, media_prefixes)

    assert_df_equality(expected_media, actual_media)
    assert_df_equality(expected_remainder, actual_remainder)


def test_clean_up_links(spark_session):
    links = spark_session.createDataFrame(
        [
            ('',),
            ('Valid_One',),
            ('another_one',),
        ],
        [
            'link',
        ],
    )
    expected = spark_session.createDataFrame(
        [
            ('Valid_One', 'valid_One'),
            ('another_one', 'another_one'),
        ],
        ['link', 'normalized_link'],
    )
    actual = pipeline.clean_up_links(links)

    assert_df_equality(expected, actual)


def test_resolve_redirects(spark_session, sections):
    redirects = spark_session.createDataFrame(
        [
            ('eswiki', 'El_día_de_la_bestia', 'El_día_de_la_bestia_(Película)'),
            ('frwiki', 'Vérité', 'La_Vérité_Est_Là'),
            ('ptwiki', 'Feijoada', 'Feijoada_Brasileira'),  # Not in `sections`
        ],
        ['wiki_db', 'page_title', 'page_redirect_title'],
    )
    expected = spark_session.createDataFrame(
        [
            # Redirected
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                0,
                '### zero ###',
                'El_día_de_la_bestia_(Película)',
                'el_día_de_la_bestia_(Película)',
            ),
            ('ptwiki', 37, 'Q101', 101, 'Presunto', 0, '### zero ###', None, None),
            (
                'ptwiki',
                37,
                'Q101',
                101,
                'Presunto',
                1,
                'one',
                'Ficheiro:Presunto_de_Monchique.WEBP',
                'Ficheiro:presunto_de_Monchique.WEBP',
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                0,
                '### zero ###',
                'George_Orwell',
                'george_Orwell',
            ),
            ('frwiki', 77, 'Q1984', 1984, '1984', 1, 'Résumé', None, None),
            # Redirected
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                2,
                'Personnages',
                'La_Vérité_Est_Là',
                'la_Vérité_Est_Là',
            ),
            ('jawiki', 610, 'Q1234', 1234, '茎茶', 0, '### zero ###', None, None),
            (
                'jawiki',
                610,
                'Q1234',
                1234,
                '茎茶',
                1,
                '1',
                'カテゴリ:Japanese_Teas',
                'カテゴリ:Japanese_Teas',
            ),
            ('arywiki', 999, 'Q4321', 4321, 'ﻁﺎﺠﻴﻧ', 0, '### zero ###', None, None),
            (
                'arywiki',
                999,
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                1,
                '1',
                'ﺖﺼﻨﻴﻓ:Moroccan_Dishes',
                'ﺖﺼﻨﻴﻓ:moroccan_Dishes',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'link',
            'normalized_link',
        ],
    )
    actual = pipeline.resolve_redirects(sections, redirects)

    assert_df_equality(expected, actual, ignore_row_order=True)


def test_gather_section_topics(spark_session, sections):
    # NOTE The tested function expects full dataframes of articles and categories,
    # but only uses 3 columns, so we focus on them for simplicity
    articles = spark_session.createDataFrame(
        [
            ('eswiki', 'Q1983', 'Entre_tinieblas'),
            # Standard `page_title` case here, non-standard one in the `sections` fixture: they should match
            ('eswiki', 'Q1995', 'El_día_de_la_bestia'),
            # Same article in 2 wikis here, French only in the `sections` fixture: French should match
            ('eswiki', 'Q4891', 'George_Orwell'),
            ('frwiki', 'Q4891', 'George_Orwell'),
            # Lower `page_title` case here, upper case in the `sections` fixture: they should match
            ('frwiki', 'Q8', 'vérité'),
        ],
        ['wiki_db', 'page_qid', 'page_title'],
    )
    categories = spark_session.createDataFrame(
        [
            # Should match
            ('jawiki', 'Q42', 'カテゴリ:Japanese_Teas'),
            # Looks like the same link, but the case-sensitive part is different from the `sections` fixture:
            # `topic_qid` should be null
            ('arywiki', 'Q1010', 'ﺖﺼﻨﻴﻓ:mOrOccAn_dIsHeS'),
            # Not in the `sections` fixture
            ('frwiki', 'Q1992', 'Catégorie:A_French_Category'),
        ],
        ['wiki_db', 'page_qid', 'page_title'],
    )
    expected = spark_session.createDataFrame(
        [
            # The original `topic_title` value from the `sections` fixture is kept
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                0,
                '### zero ###',
                'eswiki|Q666|0',
                'Q1995',
                'El_día_de_la_bestia',
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                0,
                '### zero ###',
                'frwiki|Q1984|0',
                'Q4891',
                'George_Orwell',
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                1,
                'Résumé',
                'frwiki|Q1984|1',
                None,
                None,
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                2,
                'Personnages',
                'frwiki|Q1984|2',
                'Q8',
                'vérité',
            ),
            (
                'ptwiki',
                37,
                'Q101',
                101,
                'Presunto',
                0,
                '### zero ###',
                'ptwiki|Q101|0',
                None,
                None,
            ),
            (
                'ptwiki',
                37,
                'Q101',
                101,
                'Presunto',
                1,
                'one',
                'ptwiki|Q101|1',
                None,
                'Ficheiro:Presunto_de_Monchique.WEBP',
            ),
            (
                'jawiki',
                610,
                'Q1234',
                1234,
                '茎茶',
                0,
                '### zero ###',
                'jawiki|Q1234|0',
                None,
                None,
            ),
            (
                'jawiki',
                610,
                'Q1234',
                1234,
                '茎茶',
                1,
                '1',
                'jawiki|Q1234|1',
                'Q42',
                'カテゴリ:Japanese_Teas',
            ),
            (
                'arywiki',
                999,
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                0,
                '### zero ###',
                'arywiki|Q4321|0',
                None,
                None,
            ),
            (
                'arywiki',
                999,
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                1,
                '1',
                'arywiki|Q4321|1',
                None,
                'ﺖﺼﻨﻴﻓ:Moroccan_Dishes',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'section_id',
            'topic_qid',
            'topic_title',
        ],
    )
    actual = pipeline.gather_section_topics(sections, articles, categories)

    assert_df_equality(expected, actual, ignore_row_order=True, ignore_nullable=True)


def test_compute_article_relevance(spark_session):
    columns_for_test = [
        'unique_id',
        'page_qid',
        'topic_qid',
        'tf_num',
        'tf_den',
        'tf',
        'idf_num',
        'idf_den',
        'idf',
        'tf_idf',
    ]
    # 2 wikis, 3 pages, 4 topics. Q1 occurs once, Q2 twice, etc.
    topics = spark_session.createDataFrame(
        [
            (1, 'en', 'Q42', 'Q1'),
            (2, 'en', 'Q42', 'Q2'),
            (3, 'pt', 'Q42', 'Q2'),
            (4, 'pt', 'Q42', 'Q3'),
            (5, 'pt', 'Q42', 'Q3'),
            (6, 'pt', 'Q77', 'Q3'),
            (7, 'pt', 'Q77', 'Q4'),
            (8, 'pt', 'Q77', 'Q4'),
            (9, 'pt', 'Q16', 'Q4'),
            (10, 'pt', 'Q16', 'Q4'),
        ],
        ['unique_id', 'wiki_db', 'page_qid', 'topic_qid'],
    )
    expected = spark_session.createDataFrame(
        [
            (1, 'Q42', 'Q1', 1, 5, 1 / 5, 1, 1, log(1 / 1), 1 / 5 * log(1 / 1)),
            (2, 'Q42', 'Q2', 2, 5, 2 / 5, 1, 1, log(1 / 1), 2 / 5 * log(1 / 1)),
            (3, 'Q42', 'Q2', 2, 5, 2 / 5, 3, 1, log(3 / 1), 2 / 5 * log(3 / 1)),
            (4, 'Q42', 'Q3', 2, 5, 2 / 5, 3, 2, log(3 / 2), 2 / 5 * log(3 / 2)),
            (5, 'Q42', 'Q3', 2, 5, 2 / 5, 3, 2, log(3 / 2), 2 / 5 * log(3 / 2)),
            (6, 'Q77', 'Q3', 1, 3, 1 / 3, 3, 2, log(3 / 2), 1 / 3 * log(3 / 2)),
            (7, 'Q77', 'Q4', 2, 3, 2 / 3, 3, 2, log(3 / 2), 2 / 3 * log(3 / 2)),
            (8, 'Q77', 'Q4', 2, 3, 2 / 3, 3, 2, log(3 / 2), 2 / 3 * log(3 / 2)),
            (9, 'Q16', 'Q4', 2, 2, 2 / 2, 3, 2, log(3 / 2), 2 / 2 * log(3 / 2)),
            (10, 'Q16', 'Q4', 2, 2, 2 / 2, 3, 2, log(3 / 2), 2 / 2 * log(3 / 2)),
        ],
        columns_for_test,
    )
    # Select columns relevant to the test, including a synthetic unique_id for test reproducibility
    actual = (
        pipeline.compute_relevance(topics, level='article')
        .select(*columns_for_test)
        .sort('unique_id')
    )

    # use approximate equality on the floating point columns
    assert_approx_df_equality(
        expected, actual, precision=0.00000000001, ignore_nullable=True
    )


def test_compute_section_relevance(spark_session):
    columns_for_test = [
        'unique_id',
        'section_id',
        'topic_qid',
        'tf_num',
        'tf_den',
        'tf',
        'idf_num',
        'idf_den',
        'idf',
        'tf_idf',
    ]
    # en has 2 different sections, pt has 7. Given a topic:
    # More occurrences in the same section can happen due to templates.
    # More occurrences in different sections shouldn't happen by policy, but who knows if it's enforced.
    topics = spark_session.createDataFrame(
        [
            # 1 occurrence
            (1, 'en', 'en|Q42|3', 'Q1'),
            (2, 'en', 'en|Q42|5', 'Q1'),
            (3, 'pt', 'pt|Q42|9', 'Q1'),
            (4, 'pt', 'pt|Q77|4', 'Q1'),
            # 2 occurrences in the same section
            (5, 'pt', 'pt|Q16|1', 'Q2'),
            (6, 'pt', 'pt|Q16|1', 'Q2'),
            # 3 occurrences in different sections
            (7, 'pt', 'pt|Q42|0', 'Q3'),
            (8, 'pt', 'pt|Q42|0', 'Q3'),
            (9, 'pt', 'pt|Q42|7', 'Q3'),
            # 4 occurrences in different sections
            (10, 'pt', 'pt|Q77|6', 'Q4'),
            (11, 'pt', 'pt|Q77|2', 'Q4'),
            (12, 'pt', 'pt|Q77|2', 'Q4'),
            (13, 'pt', 'pt|Q77|6', 'Q4'),
        ],
        ['unique_id', 'wiki_db', 'section_id', 'topic_qid'],
    )
    # TF-IDF values: intentionally redundant for the sake of readability (well...)
    # TF numerator, TF denominator, TF, IDF numerator, IDF denominator, IDF, TF-IDF
    en_q1 = [1, 1, 1 / 1, 2, 2, log(2 / 2), 1 / 1 * log(2 / 2)]
    pt_q1 = [1, 1, 1 / 1, 7, 2, log(7 / 2), 1 / 1 * log(7 / 2)]
    q2 = [2, 2, 2 / 2, 7, 1, log(7 / 1), 2 / 2 * log(7 / 1)]
    q3_sec0 = [2, 2, 2 / 2, 7, 2, log(7 / 2), 2 / 2 * log(7 / 2)]
    q3_sec7 = [1, 1, 1 / 1, 7, 2, log(7 / 2), 1 / 1 * log(7 / 2)]
    q4 = [2, 2, 2 / 2, 7, 2, log(7 / 2), 2 / 2 * log(7 / 2)]
    expected = spark_session.createDataFrame(
        [
            # 1 occurrence
            (1, 'en|Q42|3', 'Q1', *en_q1),
            (2, 'en|Q42|5', 'Q1', *en_q1),
            (3, 'pt|Q42|9', 'Q1', *pt_q1),
            (4, 'pt|Q77|4', 'Q1', *pt_q1),
            # 2 occurrences in the same section
            (5, 'pt|Q16|1', 'Q2', *q2),
            (6, 'pt|Q16|1', 'Q2', *q2),
            # 3 occurrences in different sections
            (7, 'pt|Q42|0', 'Q3', *q3_sec0),
            (8, 'pt|Q42|0', 'Q3', *q3_sec0),
            (9, 'pt|Q42|7', 'Q3', *q3_sec7),
            # 4 occurrences in different sections
            (10, 'pt|Q77|6', 'Q4', *q4),
            (11, 'pt|Q77|2', 'Q4', *q4),
            (12, 'pt|Q77|2', 'Q4', *q4),
            (13, 'pt|Q77|6', 'Q4', *q4),
        ],
        columns_for_test,
    )
    # Select columns relevant to the test, including a synthetic unique_id for test reproducibility
    actual = (
        pipeline.compute_relevance(topics, level='section')
        .select(*columns_for_test)
        .sort('unique_id')
    )

    # use approximate equality on the floating point columns
    assert_approx_df_equality(
        expected, actual, precision=0.00000000001, ignore_nullable=True
    )


def test_compose_output(spark_session):
    snapshot = '1984'
    # Use all columns here, since we test the final output
    scored_topics = spark_session.createDataFrame(
        [
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                0,
                '### zero ###',
                'eswiki|Q666|0',
                'Q1995',
                'El_día_de_la_bestia',
                1,
                2,
                3,
                4,
                5,
                6,
                7,
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                0,
                '### zero ###',
                'frwiki|Q1984|0',
                'Q4891',
                'George_Orwell',
                1,
                2,
                3,
                4,
                5,
                6,
                7,
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                2,
                'Personnages',
                'frwiki|Q1984|2',
                'Q8',
                'Vérité',
                1,
                2,
                3,
                4,
                5,
                6,
                7,
            ),
            (
                'jawiki',
                610,
                'Q1234',
                1234,
                '茎茶',
                1,
                '1',
                'jawiki|Q1234|1',
                'Q42',
                'カテゴリ:Japanese_Teas',
                1,
                2,
                3,
                4,
                5,
                6,
                7,
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'section_id',
            'topic_qid',
            'topic_title',
            'tf_num',
            'tf_den',
            'tf',
            'idf_num',
            'idf_den',
            'idf',
            'tf_idf',
        ],
    )
    all_topics = spark_session.createDataFrame(
        [
            # Topics that got scored above
            (
                'eswiki',
                42,
                'Q666',
                666,
                'El_Diablo',
                0,
                '### zero ###',
                'eswiki|Q666|0',
                'Q1995',
                'El_día_de_la_bestia',
            ),
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                0,
                '### zero ###',
                'frwiki|Q1984|0',
                'Q4891',
                'George_Orwell',
            ),
            # Null topics
            (
                'frwiki',
                77,
                'Q1984',
                1984,
                '1984',
                1,
                'Résumé',
                'frwiki|Q1984|1',
                None,
                'Un_Lien_Rouge',
            ),
            (
                'ptwiki',
                37,
                'Q101',
                101,
                'Presunto',
                0,
                '### zero ###',
                'ptwiki|Q101|0',
                None,
                'Não_estou_em_Wikidata',
            ),
            (
                'jawiki',
                610,
                'Q1234',
                1234,
                '茎茶',
                0,
                '### zero ###',
                'jawiki|Q1234|0',
                None,
                'ファイル:a_Japanese_Image',
            ),
            (
                'arywiki',
                999,
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                1,
                '1',
                'arywiki|Q4321|1',
                None,
                'تصویر:A_Moroccan_File',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'section_id',
            'topic_qid',
            'topic_title',
        ],
    )
    expected = spark_session.createDataFrame(
        [
            (
                snapshot,
                'eswiki',
                0,
                42,
                'Q666',
                666,
                'El_Diablo',
                0,
                '### zero ###',
                'Q1995',
                'El_día_de_la_bestia',
                7,
            ),
            (
                snapshot,
                'frwiki',
                0,
                77,
                'Q1984',
                1984,
                '1984',
                0,
                '### zero ###',
                'Q4891',
                'George_Orwell',
                7,
            ),
            (
                snapshot,
                'frwiki',
                0,
                77,
                'Q1984',
                1984,
                '1984',
                2,
                'Personnages',
                'Q8',
                'Vérité',
                7,
            ),
            (
                snapshot,
                'jawiki',
                0,
                610,
                'Q1234',
                1234,
                '茎茶',
                1,
                '1',
                'Q42',
                'カテゴリ:Japanese_Teas',
                7,
            ),
            (
                snapshot,
                'frwiki',
                0,
                77,
                'Q1984',
                1984,
                '1984',
                1,
                'Résumé',
                None,
                'Un_Lien_Rouge',
                None,
            ),
            (
                snapshot,
                'ptwiki',
                0,
                37,
                'Q101',
                101,
                'Presunto',
                0,
                '### zero ###',
                None,
                'Não_estou_em_Wikidata',
                None,
            ),
            (
                snapshot,
                'jawiki',
                0,
                610,
                'Q1234',
                1234,
                '茎茶',
                0,
                '### zero ###',
                None,
                'ファイル:a_Japanese_Image',
                None,
            ),
            (
                snapshot,
                'arywiki',
                0,
                999,
                'Q4321',
                4321,
                'ﻁﺎﺠﻴﻧ',
                1,
                '1',
                None,
                'تصویر:A_Moroccan_File',
                None,
            ),
        ],
        [
            'snapshot',
            'wiki_db',
            'page_namespace',
            'revision_id',
            'page_qid',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'topic_qid',
            'topic_title',
            'topic_score',
        ],
    )
    actual = pipeline.compose_output(scored_topics, all_topics, snapshot)

    assert_df_equality(expected, actual, ignore_schema=True)


# Copied from image suggestions:
# https://gitlab.wikimedia.org/repos/generated-data-platform/image-suggestions/-/blob/7fb9d660a43b896a3d0a5ae83651e0f4581fc7bf/tests/test_shared.py
def test_get_monthly_snapshot():
    # End of the week in this month, so monthly is last month
    assert pipeline.get_monthly_snapshot('2022-05-16') == '2022-04'
    assert pipeline.get_monthly_snapshot('2022-05-01') == '2022-04'
    assert pipeline.get_monthly_snapshot('2022-01-02') == '2021-12'
    # End of the week in next month, so monthly is this month
    assert pipeline.get_monthly_snapshot('2022-05-30') == '2022-05'
    assert pipeline.get_monthly_snapshot('2021-12-27') == '2021-12'
