#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import List

import pandas as pd  # type: ignore
import pytest

from section_topics.section_alignment_image_suggestions import (
    Page,
    Recommendation,
    SectionImages,
    combine_pages,
    filter_source_images,
    generate_image_recommendations,
    load,
    make_recommendations,
    rows_to_pages,
)


@pytest.mark.parametrize(
    'article_images,expected',
    [
        (
            '[{"index":666,"heading":"Foo","links":[],"images":[]},{"index":1984,"heading":"Bar","links":[],"images":[]}]',
            [
                SectionImages(index=666, heading='Foo', links=[], images=[]),
                SectionImages(index=1984, heading='Bar', links=[], images=[]),
            ],
        ),
        (
            '[{"index":42,"heading":"Foo","links":["Spam"],"images":["Bar.jpg"]},{"index":4891,"heading":"Baz","links":["Eggz"],"images":["Qux.jpg"]}]',
            [
                SectionImages(
                    index=42, heading='Foo', links=['Spam'], images=['Bar.jpg']
                ),
                SectionImages(
                    index=4891, heading='Baz', links=['Eggz'], images=['Qux.jpg']
                ),
            ],
        ),
    ],
)
def test_load(article_images: str, expected: List[SectionImages]) -> None:
    assert load(article_images) == expected


@pytest.mark.parametrize(
    'data,expected',
    [
        ([], []),
        (
            [
                (
                    'Q1',
                    1,
                    'Foo',
                    'enwiki',
                    '[{"index":1955,"heading":"Bar","links":["Spam"],"images":["Baz.jpg"]}]',
                )
            ],
            [
                Page(
                    item_id='Q1',
                    page_id=1,
                    page_title='Foo',
                    wiki_db='enwiki',
                    section_images=[
                        SectionImages(
                            index=1955,
                            heading='Bar',
                            links=['Spam'],
                            images=['Baz.jpg'],
                        )
                    ],
                )
            ],
        ),
    ],
)
def test_rows_to_pages(data: List[str], expected: List[Page]) -> None:
    df = pd.DataFrame(
        data, columns=['item_id', 'page_id', 'page_title', 'wiki_db', 'article_images']
    )
    assert rows_to_pages(df) == expected


@pytest.mark.parametrize(
    'source_images,target_images,expected',
    [
        (['Foo.jpg', 'Bar.jpg'], ['Bar.jpg'], ['Foo.jpg']),
        (['OOjs_UI_icon.jpg'], [], []),
    ],
)
def test_filter_source_images(
    source_images: List[str], target_images: List[str], expected: List[str]
) -> None:
    assert filter_source_images(source_images, set(target_images)) == expected


@pytest.mark.parametrize(
    'target_page,source_page,max_target_images,expected',
    [
        (
            Page(
                item_id='Q1',
                page_id=4891,
                page_title='Foo',
                wiki_db='arwiki',
                section_images=[],
            ),
            Page(
                item_id='Q1',
                page_id=1984,
                page_title='Baz',
                wiki_db='enwiki',
                section_images=[
                    SectionImages(
                        index=777, heading='Qux', links=[], images=['Quux.jpg']
                    ),
                ],
            ),
            5,
            [],
        ),
        (
            Page(
                item_id='Q1',
                page_id=666,
                page_title='Foo',
                wiki_db='afwiki',
                section_images=[
                    SectionImages(
                        index=38, heading='Bar', links=[], images=['Baz.jpg']
                    ),
                ],
            ),
            Page(
                item_id='Q1',
                page_id=999,
                page_title='Qux',
                wiki_db='ptwiki',
                section_images=[
                    SectionImages(
                        index=77,
                        heading='Quux',
                        links=[],
                        images=['Baz.jpg', 'Quuz.jpg', 'OOjs_UI_icon.jpg', 'Corge.jpg'],
                    ),
                ],
            ),
            1,
            [
                Recommendation(
                    item_id='Q1',
                    target_id=666,
                    target_title='Foo',
                    target_index=38,
                    target_heading='Bar',
                    target_wiki_db='afwiki',
                    source_heading='Quux',
                    source_wiki_db='ptwiki',
                    recommended_images=['Quuz.jpg', 'Corge.jpg'],
                )
            ],
        ),
        (
            Page(
                item_id='Q1',
                page_id=666,
                page_title='Foo',
                wiki_db='afwiki',
                section_images=[
                    SectionImages(
                        index=11, heading='Bar', links=[], images=['Baz.jpg']
                    ),
                ],
            ),
            Page(
                item_id='Q1',
                page_id=999,
                page_title='Qux',
                wiki_db='ptwiki',
                section_images=[
                    SectionImages(
                        index=42,
                        heading='Quux',
                        links=[],
                        images=['Baz.jpg', 'Quuz.jpg', 'OOjs_UI_icon.jpg', 'Corge.jpg'],
                    ),
                ],
            ),
            0,
            [],
        ),
    ],
)
def test_make_recommendations(
    target_page: Page,
    source_page: Page,
    max_target_images: int,
    expected: List[Recommendation],
) -> None:
    assert make_recommendations(target_page, source_page, max_target_images) == expected


def test_combine_pages() -> None:
    page1 = Page(
        item_id='Q1',
        page_id=666,
        page_title='Foo',
        wiki_db='afwiki',
        section_images=[
            SectionImages(
                index=13, heading='Lede_section', links=[], images=['Corge.jpg']
            ),
        ],
    )
    page2 = Page(
        item_id='Q1',
        page_id=999,
        page_title='Qux',
        wiki_db='ptwiki',
        section_images=[
            SectionImages(
                index=44, heading='Lede_section', links=[], images=['Grault.jpg']
            ),
        ],
    )
    page3 = Page(
        item_id='Q1',
        page_id=77,
        page_title='Qux',
        wiki_db='eswiki',
        section_images=[
            SectionImages(
                index=1269, heading='Lede_section', links=[], images=['Grault.jpg']
            ),
        ],
    )

    target_pages = [page1]
    source_pages = [page1, page2, page3]
    assert combine_pages(target_pages, source_pages) == [(page1, page2), (page1, page3)]

    target_pages = [page1]
    source_pages = [page1]
    assert combine_pages(target_pages, source_pages) == []


@pytest.mark.parametrize(
    'target_wiki_dbs,max_target_images,data,expected_data',
    [
        (
            ['plwiki'],
            1,
            [
                (
                    'Q1',
                    666,
                    'Foo',
                    'plwiki',
                    '[{"index":610,"heading":"Bar","links":[],"images":["Baz.jpg"]}]',
                ),
                (
                    'Q1',
                    999,
                    'Bar',
                    'urwiki',
                    '[{"index":127,"heading":"Qux","links":[],"images":["Quuz.jpg"]}, {"index":12,"heading":"Corge","links":[],"images":["Quux.jpg"]}]',
                ),
            ],
            [
                ('Q1', 666, 'Foo', 610, 'Bar', 'plwiki', 'Qux', 'urwiki', ['Quuz.jpg']),
                (
                    'Q1',
                    666,
                    'Foo',
                    610,
                    'Bar',
                    'plwiki',
                    'Corge',
                    'urwiki',
                    ['Quux.jpg'],
                ),
            ],
        ),
        (
            ['eswiki'],
            5,
            [
                (
                    'Q1',
                    1984,
                    'Foo',
                    'plwiki',
                    '[{"index":44,"heading":"Bar","links":[],"images":["Baz.jpg"]}]',
                ),
                (
                    'Q1',
                    4981,
                    'Bar',
                    'urwiki',
                    '[{"index":55,"heading":"Qux","links":[],"images":["Quz.jpg"]}]',
                ),
            ],
            [],
        ),
    ],
)
def test_generate_image_recommendations(
    target_wiki_dbs: List[str],
    max_target_images: int,
    data: List[str],
    expected_data: List[str],
) -> None:
    input_df = pd.DataFrame(
        data, columns=['item_id', 'page_id', 'page_title', 'wiki_db', 'article_images']
    )
    expected_df = pd.DataFrame(
        expected_data,
        columns=[
            'item_id',
            'target_id',
            'target_title',
            'target_index',
            'target_heading',
            'target_wiki_db',
            'source_heading',
            'source_wiki_db',
            'recommended_images',
        ],
    )
    pd.testing.assert_frame_equal(
        generate_image_recommendations(target_wiki_dbs, max_target_images, input_df),
        expected_df,
    )
